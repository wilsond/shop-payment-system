if(!window.console) {
window.console = {
log : function(str) {
//alert(str); // uncomment this to see the messages
}
};
}

// the latest version v2

$(document).ready(function() {

function startup(){

// set all selects in the order div to their defaults
// and all tickboxes in the 2013 list

$('.orderaprospectus select').val(0);
$('.types input[type="checkbox"]').prop("checked", false);

// hide everything we don't need (yet) and destroy everything that might be left over
$('.fakepbuttons').remove();
$('.reset').remove();
$('h3').hide();
$('.about').hide();
$('.types').hide();
$('input[type="submit"]').hide();

$('.orderaprospectus select').addClass('shiftoff');
// add our reset link but hide it

$('form#order_form').append('<div class="reset"><a href="#">&#215; change these choices</a></div>');

$('.reset').hide();

// loop through all selects and create their html equivalents using the select names, content and values

$('.orderaprospectus select').each(function(){

var creatediv = $(this).attr('name');

$(this).parent().append('<div class="fakepbuttons" id="'+creatediv+'"></div>');

});

$(".orderaprospectus select > option").each(function(){

var addtodiv = $(this).parent().attr('name');

if(this.value!==""){

var changetext = this.text;
changetext = changetext.replace(':', '<span class="less">');

if(changetext.indexOf("span")>-1){

changetext= changetext.replace("courses", "");
  changetext = changetext+"</span>";
}

$("#"+addtodiv).append('<a href="#'+this.value+'">'+changetext+'</a>');

}

});

// and hide all the fakebuttons

$('.fakepbuttons').hide();

$('#ugorpg').prepend("<h2>Start by choosing a level</h2>");
$('#ugyear').prepend("<h2>Choose a year</h2>");
$('#pgyear').prepend("<h2>Choose a year</h2>");

// except the first lot
$('.fakepbuttons').eq(0).show();

$('.fakepbuttons a').on('click');

$('.fakepbuttons a').click(function(){

$(this).off('click');

var whichclick = $(this).parent().attr('id');

console.log(whichclick);

$(this).addClass('clicked');

// animate the click

$('#'+whichclick+ ' a').not('.clicked').css('overflow','hidden');

$('#'+whichclick+ ' a').not('.clicked').css('border','0');
$('#'+whichclick+ ' a').not('.clicked').css('height',$(this).height());

$('#'+whichclick+ ' a').not('.clicked').animate({'width':'0'}, 300, function()

{$('#'+whichclick+ ' a').not('.clicked').css('padding','0'); $(this).remove();

});

$(this).animate({'width':'350px'}, 600);

var whichdropdown = $(this).parent().attr('id');

// IE7 passed the full url as attr href
//var whichvalue = $(this).attr('href');
//whichvalue = whichvalue.substring(1);

whichvalue = $(this).attr('href').split("#");

whichvalue = whichvalue[1];


// act on the actual select html

$('select[name="'+whichdropdown+'"] option').each(function(){

if($(this).attr('value')== whichvalue){

$(this).attr("selected", "selected");

checkupto(whichdropdown);

}

});

return false;

})

}


function transform(n){

// turns dropdowns into static blocks

n = n.split(",");

for(i=0; i<n.length; i++){

var buttontext = $(".orderaprospectus select").eq(n[i]).find(':selected').text();

var creatediv = $(".orderaprospectus select").eq(n[i]).attr('name');

$(".orderaprospectus select").eq(n[i]).parent().append('<div class="fakepbuttons" id="'+creatediv+'"></div>');

changetext = buttontext.replace(':', '<span class="less">');

if(changetext.indexOf("span")>-1){

changetext= changetext.replace("courses", "");
  changetext = changetext+"</span>";
}

$("#"+creatediv).append('<a href="#'+this.value+'" class="clicked">'+changetext+'</a>');

 $('#'+creatediv+" a.clicked").css({'width':'350px'});


}

}


function errorstartup(){

$('.about fieldset').first().before('<div class="formerrors">'+shifterrors+'</div>');

$('h3').hide();

$('.orderaprospectus select').addClass('shiftoff');

$('.types').hide();


var branch = $(".orderaprospectus select").eq(0).find(":selected").val();

console.log(branch);

if(branch=="ug"){

   var branch2 = $(".orderaprospectus select").eq(1).find(":selected").val();

   console.log(branch2);

   transform("0,1");

    
}

if(branch=="pg"){
$(".types").show();
$(".types").before("<h3>Please choose which 2013 course brochures you would like us to send you:</h3>");
$(".types").addClass("before");


   var branch2 = $(".orderaprospectus select").eq(2).find(":selected").val();

   console.log(branch2);

   transform("0");


}


$('#ugorpg').prepend("<h2>Start by choosing a level</h2>");
$('#ugyear').prepend("<h2>Choose a year</h2>");
$('#pgyear').prepend("<h2>Choose a year</h2>");


}







// start the form

if($('.formerrors').length>0){

console.log('error');

var shifterrors = $('.formerrors').html();

$('.formerrors').remove();

errorstartup();


}else{

startup();

}
// reset the form

function resetchoices(){

$('resetchoices').remove();

startup();

}

// clicking on one of our buttons





// this is the logic of the form

function checkupto(n){

var state = $('select[name="'+n+'"] option:selected').attr('value');

// clicked first button - what do we show next?

if(n == "ugorpg"){

  $('.reset').show();

  $('.reset a').click(function(){

    resetchoices();

    return false;

  });

}

var state = $('select[name="'+n+'"] option:selected').attr('value');

// first part of UG form

if(state == "ug"){

$('.fakepbuttons').eq(1).show();

}

// clicked second part of UG form, so show the rest of the form

if(n=="ugyear"){
$('.about').slideToggle('300');
$('input[type="submit"]').show();
}

// clicked only part of PG form

if(state == "pg"){
$(".types").slideToggle('300');
$(".types").before("<h3>Please choose which 2013 course brochures you would like us to send you:</h3>");
$(".types").addClass("before");
$('.about').slideToggle('300');
$('input[type="submit"]').show();
}

}

});