<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Order flowers &middot; Green Earth Flowers, your local florist in Poynton, Cheshire</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="Content-Language" content="en-gb" />
<meta name="description" content="Welcome to Green Earth Flowers, your local florist in Poynton, Cheshire, delivering to South Manchester" />
<meta name="keywords" content="delivery, local, weddings, funeral, sympathy, gifts, cards" />
<meta name="author" content="Derren Wilson" />
<meta name="sponsor" content="Green Earth Flowers" />
<meta name="copyright" content="Green Earth Flowers" />
<meta http-equiv="imagetoolbar" content="false" />
<meta name="MSSmartTagsPreventParsing" content="true" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="viewport" content="width=device-width" />

<link rel="Shortcut Icon" href="/favicon.ico" type="image/x-icon" />

<link href="http://fnt.webink.com/wfs/webink.css/?project=CA2E9E8A-8DA0-45EE-8C0D-32E817C57D4B&amp;fonts=49E90CB6-0944-72CE-A0E7-8D521C8E676C:f=AdelleSans-Light,0EFF2364-9D58-0735-E88C-990B2756000B:f=AdelleSans-Bold" rel="stylesheet" type="text/css"/>


<link rel="stylesheet" href="css/gshop.css" type="text/css" />
		
</head>
<body>

<div class="main">
<div class="header">
<div class="back"><a href="#">go back</a></div>
<h1>Green Earth Flowers</h1>
<h2>Order a bouquet for local delivery<br />or pick up from the shop</h2>
</div>

<div class="progress">
<ul>
<li><b>1</b> Delivery information</li>
<li class="todo"><b>2</b> Payment information and checkout</li>
</ul>
</div>

<!-- 

whys don't look right 
drawing of select box on firefox
can't resize select box with chrome?
add arrows for back and progress report
figure out how check now should work - slim it down - remove that section when it's done? Say 'deliver within five miles - check by postcode, then give them the outside text with link?

initial state of choose area. Should it be some instructions?
modal when bouquet chosen - turn off hovers, keep image as choice, remove other options, but allow change my mind reset.

recipient correct name?

word count for message? Max 25/30 words?

preferred delivery time only shows if the delivery is not today?
today has to check if it's before 4pm?

delivery address group boxes and have a single heading
explain delivery instructions?

sign up for newsletter?

what can we send to cardsave?

-->


<form name="contactFormA" id="contactFormA" method="post" action="readout.php" target="_self">
		
				<input type="hidden" name="MerchantID" value="GreenE-2568265" />
				<input type="hidden" name="CurrencyCode" value="826" />
				<input type="hidden" name="CountryCode" value="826" />

				<input type="hidden" name="TransactionType" value="SALE" />
				<input type="hidden" name="TransactionDateTime" value="<?php echo date('Y-m-d H:i:s P'); ?>" />
				<input type="hidden" name="OrderDescription" value="Flowers from Green Earth Flowers" />
				<input type="hidden" name="CallbackURL" value="http://localhost/g/buy/done.php" />

				<input type="hidden" name="CV2Mandatory" value="true" />
				<input type="hidden" name="Address1Mandatory" value="true" />
				<input type="hidden" name="CityMandatory" value="true" />
				<input type="hidden" name="PostCodeMandatory" value="true" />
				<input type="hidden" name="StateMandatory" value="true" />
				<input type="hidden" name="CountryMandatory" value="true" />

				

				



<div class="section">
<h3><span class="icon decide"></span> Would you like us to deliver,<br />or would you prefer to pick up from the shop?</h3>

<div class="first">
<input type="radio" name="deliveryoption" value="shop" id="shop" /> <label for="shop">Pick up</label> 

<input type="radio" name="deliveryoption" value="delivery" id="delivery"/> <label for="delivery">Delivery</label> 

</div>

</div>

<div class="section">
<h3><span class="icon shop"></span> Pick up from the shop</h3>
<p>We are open 9am &#8211; 5pm Monday to Friday. We close at 3pm on Wednesday and 4pm on Saturday.</p>
</div>

<div class="section">
<h3><span class="icon postcode"></span> Check delivery postcode</h3>
<div class="why"><p>We can deliver our hand-made bouquets within a five mile radius of our shop in Poynton, Cheshire. If your delivery is outside of our local delivery area, we recommend [affliate link].</p></div>

<div class="checker">

<select name="pc1">
<option>SK1</option>
<option>SK2</option>
<option>SK3</option>
<option>SK6</option>
<option>SK7</option>
<option>SK8</option>
<option>SK9</option>
<option>SK10</option>
<option>SK11</option>
<option selected="selected">SK12</option>
<option>SK22</option>
</select>

<input name="pc2" type="text" size="4" />
<input type="submit" value="check now" id="checking" />

</div>
</div>


<div class="aftercheck">
<div class="section">
<h3><span class="icon flowers"></span> What sort of bouquet would you like delivered?</h3>

<div class="why">
<p>If you have any special requests, for example colours or flower types, you can tell us about those in the box below.</p>

<p>If you need more than one bouquet delivered, or you&#8217;d like to discuss wedding flowers or sympathy flowers, please contact the shop. </p></div>

<div class="bouquets">
<div class="bt">
<h4>Exotic bouquet</h4>
<div class="bqimg">
<img src="stock/ml1.jpg" class="start ml1" alt="flower image" />
<img src="stock/ml2.jpg" class="ml2" alt="flower image" />
<img src="stock/ml3.jpg" class="ml3" alt="flower image" />
</div>

<div class="description">
<p>Description of exotic bouquet.</p>
</div>

<ul class="bq modern">
<li><input class="bqchoose" type="checkbox" name="bouquet_type" value="exotic_30" id="ml1" alt="flower image" /><label for="ml1"><em>&pound;30</em></label></li>
<li><input class="bqchoose" type="checkbox" name="bouquet_type" value="exotic_40" id="ml2" alt="flower image" /><label for="ml2"><em>&pound;40</em></label></li>
<li><input class="bqchoose" type="checkbox" name="bouquet_type" value="exotic_50" id="ml3" alt="flower image" /><label for="ml3"><em>&pound;50</em></label></li>
<li><input class="bqchoose" type="checkbox" name="bouquet_type" value="exotic_75" id="ml4" alt="flower image" /><label for="ml4"><em>&pound;75</em></label></li>
</ul>

</div>


<div class="bt">
<h4>Seasonal bouquet</h4>
<div class="bqimg">
<img src="stock/cl1.jpg" class="start cl1" alt="flower image" />
<img src="stock/cl2.jpg" class="cl2" alt="flower image" />
<img src="stock/cl3.jpg" class="cl3" alt="flower image" />
</div>
<div class="description">
<p>Description of seasonal bouquet.</p>
</div>

<ul class="bq country">
<li><input class="bqchoose" type="checkbox" name="bouquet_type" value="seasonal_30" id="cl1" /><label for="cl1"><em>&pound;30</em></label></li>
<li><input class="bqchoose" type="checkbox" name="bouquet_type" value="seasonal_40" id="cl2" /><label for="cl2"><em>&pound;40</em></label></li>
<li><input class="bqchoose" type="checkbox" name="bouquet_type" value="seasonal_50" id="cl3" /><label for="cl3"><em>&pound;50</em></label></li>
<li><input class="bqchoose" type="checkbox" name="bouquet_type" value="seasonal_75" id="cl4" /><label for="cl4"><em>&pound;75</em></label></li>
</ul>

</div>
</div>

<label for="special">Any special flower or colour requests for this bouquet?</label>
<textarea id="special" name="special" rows="4" cols="48"></textarea>

<p class="explain">We will do the best we can to fulfill your requests based on our current stock of flowers.</p>

</div>

<div class="section">


<h3><span class="icon recipient"></span> Who are the flowers going to?</h3>



<label for="rname">Recipients&#8217; name</label>
<input type="text" name="recipient_name" id="rname" size="50" />

<label for="rphone">Recipients&#8217; contact phone number</label>
<input type="text" id="rphone" size="20" name="recipient_phone" />

<label for="rmessage">Your message for the recipient</label>
<textarea id="rmessage" name="recipient_message" rows="4" cols="48"></textarea>


<?php 

$checktimeofday = localtime();
$checktimeofday = $checktimeofday[2];

if($checktimeofday<15){
$start = 0;
}else{
$start = 1;
}

?>

</div>

<div class="section">
<h3>Where and when are we delivering?</h3>

<h3>When would you like to pick up the bouquet?</h3>

<div class="delivery">
<h4>Delivery address</h4>
<!-- <label for="raddress1">Delivery address <em>line one</em></label> -->
<p><input type="text" name="delivery_address1" size="50" id="delivery_address1" /></p>
<p>
<!-- <label for="raddress2">Delivery address <em>line two</em></label>-->
<input type="text" name="delivery_address2" size="50" id="delivery_address2" />
</p>
<p>
<!-- <label for="raddress3">Delivery address <em>line three</em></label>-->
<input type="text" name="delivery_address3" size="50" id="delivery_address3" />
</p>

</div>

<label for="delivery_postcode">Delivery postcode</label>
<input type="text" size="10" name="delivery_postcode" id="delivery_postcode" />


<div class="delivery">
<label for="delivery_date">Delivery date</label>

<select name="delivery_date" id="delivery_date">

<?php



$date = time();



for($i=$start; $i<20;$i++){

$newDate = strtotime('+'.$i.' days',$date);

$checkdate = date('l, jS F',$newDate);
$backenddate = date('d-m-y', $newDate);


if(strstr($checkdate,"Sunday")){

}else{
if($i==0){$today = "Today: ";}else{$today ="";}
echo '<option value="'.$backenddate.'">'.$today.$checkdate.'</option>';
}

}
?>

</select>


</div>


<label for="delivery_instruction">Any special delivery instructions?</label>
<textarea id="delivery_instruction" name="delivery_instruction" rows="4" cols="48"></textarea>






</div>

</div>

<div class="section">

<h3><span class="icon orderer"></span> Your details</h3>

<label for="order_name">Your name</label>
<input type="text" name="order_name" id="order_name" size="50" />



							
							<label for="order_name">Your name</label>
				<input name="CustomerName" value="John Watson" />

				<label for="Address1">Address line one</label>
				<input name="Address1" value="32 Edward Street" /><br />
				<label for="Address2">Address line two</label>
				<input name="Address2" value="" /><br />
				<label for="Address3">Address line three</label>
				<input name="Address3" value="" /><br />
				<label for="Address4">Address line four</label>
				<input name="Address4" value="" /><br />
				<label for="city">Your city</label>
				<input name="City" value="Camborne" /><br /> 
				<label for="state">State</label>
				<input name="State" value="Cornwall" /><br />
				<label for="postcode">Postcost</label>
				<input name="PostCode" value="TR14 8PA" /><br /> 
				


<label for="order_phone">Your contact phone number</label>
<input type="text" name="order_phone" id="order_phone" size="20" />
<p class="explain">We need your phone number in case we need to contact you about this order. We will take your contact address as part of the payment process.</p>


<label for="order_email">Your contact email address</label>
<input type="text" name="order_email" id="order_email" size="20" />
</div>

<div class="section">


<input type="submit" value="Continue"/>


</form>

</div>

<?php



// get postcode authenticated

// get flower order

// get recipient instructions

// get florist instructions

// handover to worldpay





?>
</div>
</div>


<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js" type="text/javascript"></script>
<script src="js/shop.js" type="text/javascript"></script>


</body>
</html>