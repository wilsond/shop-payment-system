<?php
if (isset($_POST['submit']) && $_POST['submit'] == "Rate now") {
	try {
		$newFeedback = Food::validate($_POST);
		if (is_a($newFeedback, 'Validator')) {
			$showErrors = true;
		} else {
			$newFeedback->sendEmail();
			header("Location: thanks/index.php");
			exit();
		}
	} catch( Exception $e ) {

	}
}
?>

<div class="fooddrinkcopy feedback">
    <div class="pagecopy">
        <?php
        if(isset($showErrors) && $showErrors == true) {
        	echo $newFeedback->getErrorList();
        }
        ?>
        <form action="<?php echo $_SERVER['PHP_SELF']; ?>" id="facilities_feedback_form" method="post">
            <table id="rating">
                <tbody>
                    <tr>
                        <td><label for="name">Name:</label></td>
                        <td><input name="name" type="text" id="name" size="30" maxlength="30" value="<?php echo ((isset($_POST['name']) && $_POST['name'] != '')?$_POST['name']:'') ?>" /></td>
                    </tr>
                    <tr>
                        <td><label for="email">Email (please enter a valid email address):</label></td>
                        <td><input name="email_address" type="text" id="email" size="30" maxlength="50" value="<?php echo ((isset($_POST['email_address']) && $_POST['email_address'] != '')?$_POST['email_address']:'') ?>" /></td>
                    </tr>
                    <tr>
                        <td>Which Catering Outlet?</td>
                        <td><select id="catering" name="catering">
                                <option value="all-saints-snack-bar" <?php echo ((isset($_POST['catering']) && $_POST['catering'] == 'all-saints-snack-bar')?'selected="selected"':'') ?>>All Saints Snack Bar</option>
                                <option value="manton-bistro" <?php echo ((isset($_POST['catering']) && $_POST['catering'] == 'manton-bistro')?'selected="selected"':'') ?>>Geoffrey Manton Bistro</option>
                                <option value="john-dalton-food-zone" <?php echo ((isset($_POST['catering']) && $_POST['catering'] == 'john-dalton-food-zone')?'selected="selected"':'') ?>>John Dalton Food Zone</option>
                                <option value="mabel-tylecote-green-room" <?php echo ((isset($_POST['catering']) && $_POST['catering'] == 'mabel-tylecote-green-room')?'selected="selected"':'') ?>>Mabel Tylecote Green Room</option>
                                <option value="crewe-student-zone" <?php echo ((isset($_POST['catering']) && $_POST['catering'] == 'crewe-student-zone')?'selected="selected"':'') ?>>Crewe Student Zone</option>
                                <option value="valentinos-cafe" <?php echo ((isset($_POST['catering']) && $_POST['catering'] == 'valentinos-cafe-bar')?'selected="selected"':'') ?>>Valentino&#8217;s Caf&#233;</option>
                                <option value="didsbury-food-zone" <?php echo ((isset($_POST['catering']) && $_POST['catering'] == 'didsbury-food-zone')?'selected="selected"':'') ?>>Didsbury Food Zone</option>
                                <option value="gaskell-cafe" <?php echo ((isset($_POST['catering']) && $_POST['catering'] == 'gaskell-cafe')?'selected="selected"':'') ?>>Gaskell Caf&#233;</option>
                                <option value="hollings-cafe" <?php echo ((isset($_POST['catering']) && $_POST['catering'] == 'hollings-cafe')?'selected="selected"':'') ?>>Hollings Caf&#233;</option>
                                <option value="hub-cafe" <?php echo ((isset($_POST['catering']) && $_POST['catering'] == 'hub-cafe')?'selected="selected"':'') ?>>The Hub Caf&#233;</option>
                                <option value="hub-kitchen" <?php echo ((isset($_POST['catering']) && $_POST['catering'] == 'hub-kitchen')?'selected="selected"':'') ?>>The Hub Kitchen</option>
                                <option value="crewe-substop" <?php echo ((isset($_POST['catering']) && $_POST['catering'] == 'crew-substop')?'selected="selected"':'') ?>>Crewe Substop</option>
                                <option value="holden-pop-up-cafe" <?php echo ((isset($_POST['catering']) && $_POST['catering'] == 'holden-pop-up-cafe')?'selected="selected"':'') ?>>Holden Pop-up Caf&eacute;</option>
                                <option value="art-school-cafe" <?php echo ((isset($_POST['catering']) && $_POST['catering'] == 'art-school-cafe')?'selected="selected"':'') ?>>Art School Caf&eacute;</option>
                                <!--<option value="hospitality" <?php echo ((isset($_POST['catering']) && $_POST['catering'] == 'hospitality')?'selected="selected"':'') ?>>Hospitality</option>-->
                            </select></td>
                    </tr>
                    <tr>
                        <td><label for="c_food">How do you rate the quality of food?</label></td>
                        <td><input name="c_food" type="radio" value="1" class="star" <?php echo ((isset($_POST['c_food']) && $_POST['c_food'] == '1')?'checked="checked"':'') ?>/>
                            <input name="c_food" type="radio" value="2" class="star" <?php echo ((isset($_POST['c_food']) && $_POST['c_food'] == '2')?'checked="checked"':'') ?>/>
                            <input name="c_food" type="radio" value="3" class="star" <?php echo ((isset($_POST['c_food']) && $_POST['c_food'] == '3')?'checked="checked"':'') ?>/>
                            <input name="c_food" type="radio" value="4" class="star" <?php echo ((isset($_POST['c_food']) && $_POST['c_food'] == '4')?'checked="checked"':'') ?>/>
                            <input name="c_food" type="radio" value="5" class="star" <?php echo ((isset($_POST['c_food']) && $_POST['c_food'] == '5')?'checked="checked"':'') ?>/></td>
                    </tr>
                    <tr>
                        <td><label for="c_money">How do you rate our prices?</label></td>
                        <td><input name="c_money" type="radio" value="1" class="star" <?php echo ((isset($_POST['c_money']) && $_POST['c_money'] == '1')?'checked="checked"':'') ?>/>
                            <input name="c_money" type="radio" value="2" class="star" <?php echo ((isset($_POST['c_money']) && $_POST['c_money'] == '2')?'checked="checked"':'') ?>/>
                            <input name="c_money" type="radio" value="3" class="star" <?php echo ((isset($_POST['c_money']) && $_POST['c_money'] == '3')?'checked="checked"':'') ?>/>
                            <input name="c_money" type="radio" value="4" class="star" <?php echo ((isset($_POST['c_money']) && $_POST['c_money'] == '4')?'checked="checked"':'') ?>/>
                            <input name="c_money" type="radio" value="5" class="star" <?php echo ((isset($_POST['c_money']) && $_POST['c_money'] == '5')?'checked="checked"':'') ?>/></td>
                    </tr>
                    <tr>
                        <td><label for="c_quality">How do you rate the service provided by our staff?</label></td>
                        <td><input name="c_quality" type="radio" value="1" class="star" <?php echo ((isset($_POST['c_quality']) && $_POST['c_quality'] == '1')?'checked="checked"':'') ?>/>
                            <input name="c_quality" type="radio" value="2" class="star" <?php echo ((isset($_POST['c_quality']) && $_POST['c_quality'] == '2')?'checked="checked"':'') ?>/>
                            <input name="c_quality" type="radio" value="3" class="star" <?php echo ((isset($_POST['c_quality']) && $_POST['c_quality'] == '3')?'checked="checked"':'') ?>/>
                            <input name="c_quality" type="radio" value="4" class="star" <?php echo ((isset($_POST['c_quality']) && $_POST['c_quality'] == '4')?'checked="checked"':'') ?>/>
                            <input name="c_quality" type="radio" value="5" class="star" <?php echo ((isset($_POST['c_quality']) && $_POST['c_quality'] == '5')?'checked="checked"':'') ?>/></td>
                    </tr>
                    <tr>
                        <td><label for="c_look">How do you rate the appearance of this outlet?</label></td>
                        <td><input name="c_look" type="radio" value="1" class="star" <?php echo ((isset($_POST['c_look']) && $_POST['c_look'] == '1')?'checked="checked"':'') ?>/>
                            <input name="c_look" type="radio" value="2" class="star" <?php echo ((isset($_POST['c_look']) && $_POST['c_look'] == '2')?'checked="checked"':'') ?>/>
                            <input name="c_look" type="radio" value="3" class="star" <?php echo ((isset($_POST['c_look']) && $_POST['c_look'] == '3')?'checked="checked"':'') ?>/>
                            <input name="c_look" type="radio" value="4" class="star" <?php echo ((isset($_POST['c_look']) && $_POST['c_look'] == '4')?'checked="checked"':'') ?>/>
                            <input name="c_look" type="radio" value="5" class="star" <?php echo ((isset($_POST['c_look']) && $_POST['c_look'] == '5')?'checked="checked"':'') ?>/></td>
                    </tr>
                    <tr>
                        <td><label for="comments">What is your reason for this rating?</label></td>
                        <td><textarea name="comments" cols="28" rows="8" id="comments"><?php echo ((isset($_POST['comments']) && $_POST['comments'] != '')?$_POST['comments']:''); ?></textarea></td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td><input type="submit" name="submit" value="Rate now" /></td>
                    </tr>
                </tbody>
            </table>
        </form>
    </div>
</div>


<?php
class Food extends CoreObject {

	public function __construct($infoArray = array()) {
		// call parent constructor
		parent::__construct($infoArray);
	}

	public static function validate($values = array()) {
		$values = array_merge(
			array(
				'name' => '',
				'email_address' => '',
				'comments' => '',
			),
			$values
		);

		$fields = array(
			array("Your Name", "Empty", $values['name']),
			array("Your Email address", "Empty", $values['email_address']),
			array("Your Email address", "EmailAddress", $values['email_address']),
			array("Comments", "Empty", $values['comments']),
		);

		$validation = new Validator($fields);
		$validation->process();

		if ($validation->isError()) {
			return $validation;
		} else {
			return new Food($values);
		}
	}

	public function sendEmail() {
		$subject = "Catering Feedback Submission";
		$headers = "From: " . $this->name . " <" . $this->email . ">\n";
		$headers .= "Reply-To: " . $this->name . " <" . $this->email . ">\n";

		$body = "The following information has been submitted via the feedback form at http://www.mmu.ac.uk/food/rateourvenues/\n\n" .
"Name: {$this->name}\n" .
"E-mail: {$this->email_address}\n" .
"Outlet: {$this->catering}\n\n" .
"Quality of Food: {$this->c_food}\n\n" .
"Prices: {$this->c_money}\n\n" .
"Service provided: {$this->c_quality}\n\n" .
"Outlet appearance: {$this->c_look}\n\n" .
"------------------------- Comments -------------------------\n\n" .
$this->comments .
"\n\n------------------------------------------------------------\n\n";
		if (!mail('facilitiesadmin@mmu.ac.uk', $subject, $body, $headers)) {
			throw new Exception('Email failed to send');
		}
	}
}
?>


<?php

class Validator {

    // define properties
    private $_errorList;
    private $fields;

    // define methods
    // constructor
    public function __construct($fields) {
        $this->resetErrorList();
        $this->fields = $fields;
    }

    public function process() {
        foreach($this->fields as $key=>$value) {
            $this->isSpammy($value);
            $method = "is".$value[1];
            $this->$method($value);
        }
    }

    // initialize error list
    private function resetErrorList() {
        $this->_errorList = array();
    }

    private function isSpammy($value) {
        if(is_string($value[2])) {
            if (stristr($value[2],'http') || stristr($value[2],'www')) {
                $this->addError($value, "must not be invalid");
            }
        }
    }

    // check whether input is empty
    private function isEmpty($value) {
        if(!isset($value[2]) || trim($value[2]) == '') {
            $this->addError($value, "must not be empty");
        }
    }

    // check whether input certain length
    private function isLength($value) {
        if(strlen($value[2]) < $value[3]) {
            $this->addError($value, "must be at least 8 characters");
        }
    }

    // check whether input is empty
    private function isSelected($value) {
        if(!isset($value[2]) || trim($value[2]) == '') {
            $this->addError($value, "must be selected");
        }
    }

    // check whether input array has a value
    private function isEmptyArray($value = null) {
        if(!isset($value[2]) || empty($value[2]) || !is_array($value[2])) {
            $this->addError($value, "must have at least one option selected");
        }
    }

    // check whether input is empty
    private function isEmptyTime($value) {
        if(!isset($value[2][0]) || trim($value[2][0]) == '' || !isset($value[2][1]) || trim($value[2][1]) == '') {
            $this->addError($value, "must be set correctly");
        }
    }

    // check whether input is a string
    private function isString($value) {
        return is_string($value);
    }

    // check whether input is a number
    private function isNumber($value) {
        if(!is_numeric($value[2])) {
            $this->addError($value, "must be a number");
        }
    }

    // check whether input is an integer
    private function isInteger($value) {
        return (intval($value) == $value) ? true : false;
    }

    // check whether input is alphabetic
    private function isAlpha($value) {
        return preg_match('/^[a-zA-Z]+$/', $value);
    }

    // check whether input is within a numeric range
    private function isWithinRange($value, $min, $max) {
        return (is_numeric($value) && $value >= $min && $value <= $max) ? true : false;
    }

    // check whether input is a valid email address
    private function isEmailAddress($value) {
    	if((!trim($value[2]) == '') && (!preg_match("/^[_\.0-9a-zA-Z-]+@([0-9a-zA-Z][0-9a-zA-Z-]+\.)+[a-zA-Z]{2,6}$/i", $value[2]))) {
        //if((!trim($value[2]) == '') && (!eregi('^([a-z0-9])+([\.a-z0-9_-])*@([a-z0-9_-])+(\.[a-z0-9_-]+)*\.([a-z]{2,6})$', $value[2]))) {
            $this->addError($value, "must be a valid address");
        }
    }

    private function isAssertEqual($value) {
        if($value[2][0] != $value[2][1]) {
            $this->addError($value, "must match");
        }
    }

    // check whether input is a valid MMU email address
    private function isMmuEmailAddress($value) {
        if(substr($value[2], -10) != '@mmu.ac.uk') {
            $this->addError($value, "must be a valid MMU email address");
        }
    }

    private function isPostcode($value) {
        $postcode = strtoupper(str_replace(' ', '', $value[2]));
        if(!preg_match("/^[A-Z]{1,2}[0-9]{2,3}[A-Z]{2}$/", $postcode) || preg_match("/^[A-Z]{1,2}[0-9]{1}[A-Z]{1}[0-9]{1}[A-Z]{2}$/", $postcode) || preg_match("/^GIR0[A-Z]{2}$/", $postcode)) {
            $this->addError($value, "must be a valid UK Postcode");
        }
    }

    // check if a value exists in an array
    private function isInArray($array, $value) {
        return in_array($value, $array);
    }

    private function isNotValue($value) {
        if($value[2] == $value[3]) {
            $this->addError($value, "must be valid entry");
        }
    }

    // add an error to the error list
    private function addError($value, $message) {
        $this->_errorList[] = array('title' => $value[0], 'message' => $message);
    }

    // check if errors exist in the error list
    public function isError() {
        return (sizeof($this->_errorList) > 0) ? true : false;
    }

    // return the error list to the caller
    public function getErrorList($error_message = false) {
        if(!$error_message) {
            $errors_html = "<div class=\"formerrors\"><p><strong>There was a problem with the information you provided</strong> and your form hasn&#8217;t been sent. Please resubmit the form after checking these areas:</p>";
        } else {
            $errors_html = $error_message;
        }
        $errors_html .= "<ul>";
        foreach ($this->_errorList as $e)
        {
            $errors_html .= "<li>" . $e['title'] . ": " . $e['message'] . "</li>";
        }
        $errors_html .=  "</ul></div>";
        return $errors_html;
    }

    // return field error
    public function getFieldError($field) {
        if(isset($this->_errorList[$field]))
        return $this->_errorList[$field]['message'];
    }

    // end class definition
}
?>
