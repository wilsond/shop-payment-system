<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Order flowers &middot; Green Earth Flowers, your local florist in Poynton, Cheshire</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="Content-Language" content="en-gb" />
<meta name="description" content="Welcome to Green Earth Flowers, your local florist in Poynton, Cheshire, delivering to South Manchester" />
<meta name="keywords" content="delivery, local, weddings, funeral, sympathy, gifts, cards" />

<meta name="author" content="Derren Wilson" />
<meta name="sponsor" content="Green Earth Flowers" />
<meta name="copyright" content="Green Earth Flowers" />
<meta http-equiv="imagetoolbar" content="false" />
<meta name="MSSmartTagsPreventParsing" content="true" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="viewport" content="width=device-width" />


<link rel="Shortcut Icon" href="/favicon.ico" type="image/x-icon" />


<link href="http://fnt.webink.com/wfs/webink.css/?project=CA2E9E8A-8DA0-45EE-8C0D-32E817C57D4B&fonts=49E90CB6-0944-72CE-A0E7-8D521C8E676C:f=AdelleSans-Light,29AEAE82-9A63-C919-5496-92FEA65ACA5E:f=AdelleSans-Thin,0454C3FD-6206-9AB1-8FC7-52A41E1F6AB8:f=AdelleSans-ExtraBold,9773ABFB-EF93-0C1B-AE14-35A7DD420754:f=Theinhardt-UltraLight,864889ED-8E73-7E19-00E2-BBE0F997E58C:f=Theinhardt-Thin,92F3D8FF-2210-715D-CF2F-A674FCB8BA5B:f=Adelle-Thin,740CE5DE-8D8E-10DA-A267-38409C39EF73:f=DadaGrotesk-Book,0EFF2364-9D58-0735-E88C-990B2756000B:f=AdelleSans-Bold,35A1FBC1-3258-7D02-4478-96C95577991A:f=AdelleWeb-Light,BA766C3D-9F83-4950-AFCD-AD9F2BF5CEAB:f=Theinhardt-Regular,B3E22F30-5582-5F27-10C0-05C3621CD3DE:f=AdelleWeb-Bold,646F65BB-7FDB-DF82-711F-897188F11EE8:f=AdelleSans-SemiBold,1176510B-8371-226C-F4C1-A50E8F873A71:f=AdelleSans-Regular,2163C322-3CDA-95AB-9E42-E05BE193096D:f=DadaGrotesk-Medium,37437296-19E9-5916-9448-2D4ADE086DC5:f=AdelleWeb-Regular,2BBCE881-EA03-ACF4-900E-28357A72ADEF:f=AdelleWeb-SemiBold,F1909AAE-4087-F011-EEAD-AB047C3C6A43:f=Theinhardt-Hairline,008579D7-00D8-1E34-1306-843EC6BC82EA:f=Theinhardt-Light,3F34461C-FB56-CA82-6BDE-835B10153032:f=AdelleWeb-Heavy,4C3B1F7C-2466-6E77-F306-33C44A47930A:f=DadaGrotesk-Heavy" rel="stylesheet" type="text/css"/>


<link rel="stylesheet" href="css/gshop.css" type="text/css" />

		
</head>
<body>
<div class="main">
<div class="header">
<div class="back"></div>
<h1>Green Earth Flowers</h1>
<h2>Order a bouquet<br />for local delivery</h2>
</div>


<div class="success">
DONE!

</div>

<pre>
<?php

print_r($_GET);

?>
</pre>

</body>
</html>