<?php

$flower_options = Array(

Array(
	'name'=>'Exotic Bouquet',
	'price'=>'30', //in pounds
	'identifier'=>'exotic'),

Array(
	'name'=>'Exotic Bouquet',
	'price'=>'40',
	'identifier'=>'exotic'),

Array(
	'name'=>'Exotic Bouquet',
	'price'=>'50',
	'identifier'=>'exotic'),

Array(
	'name'=>'Exotic Bouquet',
	'price'=>'75',
	'identifier'=>'exotic'),


Array(
	'name'=>'Seasonal Bouquet',
	'price'=>'30',
	'identifier'=>'seasonal'),

Array(
	'name'=>'Seasonal Bouquet',
	'price'=>'40',
	'identifier'=>'seasonal'),

Array(
	'name'=>'Seasonal Bouquet',
	'price'=>'50',
	'identifier'=>'seasonal'),

Array(
	'name'=>'Seasonal Bouquet',
	'price'=>'75',
	'identifier'=>'seasonal')

);

?>