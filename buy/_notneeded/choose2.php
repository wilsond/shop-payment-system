<html>
<head>
	<style type="text/css">
	body {font-family:'helvetica neue'; font-size: 14px;}


.chooser {width: 200px; height:200px; position:relative; float:left; margin-bottom: 35px;}
.chooser+.chooser {margin-left: 14px;}
.chooser img {position:absolute; top: 0; width: 200px; height:200px; left:0; background: #eee;}
.chooser span.bt {font-weight:bold; font-size: 1.1em; z-index: 1000; line-height:1;position:absolute; top: 10px; left: 10px; margin:0; padding:0; width: 180px;}
.chooser a {color: #222; text-decoration: none;}

.chooser.selected {background: #c30; color: #fff;}
.chooser.selected img {opacity: 0.2;}

.options {width: 600px; padding: 0 15px 30px 15px; clear:both; overflow:hidden;}
.options .about {width: 200px; float:left; }
.options .about p {margin:0; padding:0;}
.options img {width: 380px; height: 380px; background: #eee; float:right;}

.options ul {margin:0; padding:0; list-style-type: none; width: 200px; float:left; margin-top: 20px;}
.options li {padding: 3px; }
.options li+li {border-top:1px solid #ddd;}

.options li span {display:inline-block; width: 80px;}
.options li em {display:inline-block; width: 50px; background: #ff4200; border-radius: 0.2em; text-align:center; font-style: normal; font-weight:bold; color: #fff; float:right;}


	.away {position:absolute; top: 0; right:0;}
	.bigchoose {width: 300px; float:left; margin-right: 20px;}
	.bigchoose h2 {margin:0; padding:0;}
	.chooseme {width: 300px; height: 32px; background: #fff;  margin: 0 0 0 0; line-height:32px;  overflow:hidden; border-bottom:1px solid #ddd;}

.bigchoose img {width: 300px; height: 150px; background: #eee; margin: 1em 0;}
.chooseme em {width: 180px; display:inline-block; font-style:normal; padding-left: 7px;}

.chooseme.notavailable { color: #ddd;}

	.available { }
	.available.hovered {color: #ff4200; cursor:pointer;}
	.available.hovered .button {background: #ad4303;}
	

	.available.selected {color: #222;}
	.available.selected .button {background: #000;}
	.available.hovered.selected { cursor:default;}



	a.changeselection {margin: 1em 0; float:left; clear:both; display:block; background: #ddd; border-radius: 0.2em; padding: 7px; width: 286px; font-weight:bold; color: #222; text-decoration: none; text-align: center;}

.button {background: #ff4200; text-transform: uppercase; width: 50px; border-radius: 0.2em; color: #fff; float:right; margin-right: 14px; text-align:center; font-size: 0.9em; line-height: 24px; margin-top: 4px;}
.button.off {background: #ddd;}


	</style>
</head>
<body>
<form action="test.php" method="get">
<h3>Choose your bouquet heading</h3>


<?php

include('_floweroptions2.php');

// output chooser boxes

foreach($flower_categories as $fc){

?>

<div class="chooser" id="<?php echo $fc['cat_selector']; ?>">
<a href="#<?php echo $fc['cat_selector']; ?>">
<span class="bt"><?php echo $fc['category']; ?></span>
<img src="<?php echo $fc['cat_image']; ?>.jpg" alt="Example of <?php echo $fc['category']; ?>" />
</a>
</div>

<?php

}


foreach($flower_categories as $fc){

?>

<div class="options" id="<?php echo $fc['cat_selector']; ?>">

<img src="<?php echo $fc['cat_image']; ?>_detail.jpg" alt="Example of <?php echo $fc['category']; ?>" />

<div class="about">
<?php echo $fc['cat_description']; ?>
</div>

<div class="buyoptions">
<ul>
<?php 

$prices = explode(',', $fc['cat_optionsandprices']);

foreach ($prices as $p){

$p = explode('|', $p);

echo "<li><span>".$p[1]."</span> <b>&pound;".$p[0]."</b> ";



echo '<input name="flowerorder" value="'.$fc['cat_selector'].'_'.$p[1].'_'.$p[0].'" type="radio" /><em>BUY</em>';

echo "</li>";

}

?>
</ul>
</div>

</div>
<?php
}
?>


<input type="submit" value="test" />
</form>



<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function() {

$('.options').hide();

var visibleoption  = 0; // for initial state

$('.chooser').click(function(){

if(visibleoption==0){

var target = $(this).attr('id');

$('.options#'+target).slideDown('300');

$(this).addClass('selected');

visibleoption = 1; //only do this once

}else{

if($(this).hasClass('selected')){return false;} // clicked the same button

console.log('hello');

var tobehidden = $('.chooser.selected').attr('id');

var tobeshown = $(this).attr('id');

console.log(tobehidden+" "+tobeshown);

$('.options#'+tobehidden).slideUp('100', function(){

$('.chooser.selected').removeClass('selected');

$('.chooser#'+tobeshown).addClass('selected');

$('.options#'+tobeshown).slideDown('300');

});



//var target = $(this).attr('id');

//$('.options#'+target).slideDown('300');

}

return false;

});




});
</script>
</body>
</html>