<?php

// the carets ^ are so that we can loop through the error fields and match them with jquery

function checkform($empty, $human){

global $errors;

$empty = split(",", $empty);

$human = split(",", $human);

$i =0;
foreach ($empty as $e){

if(isset($_POST[$e])&&$_POST[$e]!==''){
$i++;}else{

array_push($errors, $human[$i]."^".$empty[$i]);

$i++;

}

}

}


function checkemailform($isemail, $human){

global $errors;

$isemail = split(",", $isemail);
$human = split(",", $human);

$i =0;
foreach ($isemail as $e){

if(isset($_POST[$e])&&$_POST[$e]!==''){
$i++;}else{

array_push($errors, $human[$i]."^".$isemail[$i]);

$i++;

}

}

}

function checkrequiredfields($postvars){

global $errors;

if(isset($postvars['Fulfillment']) && $postvars['Fulfillment']!==''){

if($postvars['Fulfillment']=='pickup'){ 

$checkisnotempty="FlowerOption,RequiredDate,CustomerName,OrderPhone,OrderEmail,FlowerOption";
$humanmessages="Please choose some flowers,Please tell us when you need the flowers,Please enter your name,Please enter your phone number,Please enter your email address,Please choose some flowers";

checkform($checkisnotempty,$humanmessages);

$checkisemail="OrderEmail";
$humanmessagesemail="Please enter a valid email address";

checkemailform($checkisemail,$humanmessagesemail);


}

if($postvars['Fulfillment']=='delivery'){ 

$checkisnotempty="FlowerOption,RecipientName,RecipientPhone,RequiredDate,DeliveryAddress1,DeliveryPostcode2,CustomerName,OrderPhone,OrderEmail,FlowerOption";

$humanmessages="Please choose some flowers,Please enter a recipient name,Please enter a recipient phone number,Please tell us when you need the flowers,Please enter this line of the address,Please enter this line of the address,Please enter your name,Please enter your phone number,Please enter your email address,Please choose some flowers";

checkform($checkisnotempty,$humanmessages);

$checkisemail="OrderEmail";

$humanmessagesemail="Please enter a valid email address";

checkemailform($checkisemail,$humanmessagesemail);


}

}

else

{

Array_push($errors, "You need to choose pickup or delivery^Fulfillment");

}


}

function checkpostcodeOK($postvars){

global $errors;



include('_postcodechecker.php');

$possible_postcode = $postvars['DeliveryPostcode1'].' '.$postvars['DeliveryPostcode2'];

$is_correct_postcode = checkpostcode($possible_postcode);

if($is_correct_postcode == 0){

Array_push($errors, "The postcode doesn&#8217;t seem to be in our delivery area^DeliveryPostcode2");

}

return $is_correct_postcode;

}





?>