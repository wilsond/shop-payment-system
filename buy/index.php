<?php 

// html for the page

include("../includes/build_shop.php"); 

// THE USER ARRIVES AT THE PAGE

// we check if the formsubmit variable has been found in the post array. If not, that means they've not clicked the form submit button.

if(!isset($_POST['formsubmit'])){

include("_floweroptions.php"); // what people can buy

pageheader('Order flowers', 'buy');

pagenav('buy');

include("_orderform.php"); 

pagefooter();

}else{

// THE USER HAS INTERACTED WITH THE PAGE, WE NEED TO CHECK WHAT THEY'VE DONE

$CallbackURL = 'http://localhost:8888/gef/buy/finished.php'; // where users are pointed after purchase

include('_checkform.php'); // form checking functions

include("_floweroptions.php"); // what people can buy

$errors = Array(); // if this is still empty at the end, we assume they've filled the form in correctly

checkrequiredfields($_POST);

$is_correct_postcode='';

if(isset($_POST['Fulfillment'])&&($_POST['Fulfillment']=='delivery')){


$is_correct_postcode = checkpostcodeOK($_POST);

}

$errors = array_filter($errors,'strlen'); // remove empty array entries

if(count($errors)==0){

// success, you can go pay now

pageheader('Order flowers', 'buy');

pagenav('buy');

include("_orderreadout.php");

pagefooter();


}
else
{

// failure, try again.

pageheader('Order flowers', 'buy');

pagenav('buy');

$errorprintout="<div class=\"errors\"><h3>We need more information to complete your order:</h3><ul>";

$jqueryerrorpickup="";

foreach($errors as $e){

$e = split("\^", $e); 

	$errorprintout.="<li>".$e[0]."</li>";
	$jqueryerrorpickup.="<span class=\"invisible errorhighlight\">".$e[1]."</span>";

}

$errorprintout.="</ul></div>";

include("_orderform.php"); 

pagefooter();

}

// END TESTING USER INTERACTION

}

?>




