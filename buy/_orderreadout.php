<?php

//print_r($_POST);

// let us make a note in the database of what they've ordered

include("_addrecordtodatabase.php");

$orderNumber = addrecord($_POST);

// figure out where it's going and how much it will cost.

$delivery_postcode = $_POST['DeliveryPostcode1']. ' '. $_POST['DeliveryPostcode2'];

$what_bouquet =  $_POST['FlowerOption']; 

$what_bouquet = split("_", $what_bouquet);

$charge_customer = $what_bouquet[2]*100; // flower cost

$shop_description = $what_bouquet[0];

$orderID='gef_00'.$orderNumber; // order number

// delivery is in area but not free

$notices = '';

if($is_correct_postcode == 1){

$add_delivery_charge = 500; // five pounds in pence;

$notices = "We will added a <strong>&pound;5</strong> delivery charge,<br />so the total amount charged to your card on the next screen will be <strong>&pound".($charge_customer+$add_delivery_charge)*0.01."</strong>.";


}

// delivery is free

if($is_correct_postcode == 2){

$add_delivery_charge = 0; // free

$notices = "There is no delivery charge for this order";

$charge_customer = $charge_customer+$add_delivery_charge;


}

// tell them what's going on.

echo '<div class="main">
<div class="header">';

echo '<div class="progress">
<ul>
<li><b>1</b> Order information</li>
<li class="todo"><b>2</b> Payment information and checkout</li>
<li class="todo"><b>3</b> Done!</li>
</ul>
</div>';

echo "<h2>That all looks fine.</h2><p class=\"confirm\">Just to confirm, you have requested:</p>";

echo "<div class=\"whatordered\"><p>A <strong>&pound;".$charge_customer*0.01."</strong> ".ucfirst($shop_description)." bouquet</p>";

$extrainfo = $_POST['Fulfillment'];

$humandate = date("l, jS F Y", strtotime($_POST['RequiredDate']));

$translatetime = Array(

'9-11'=>'9am and 11am',
'11-1'=>'11am and 1pm',
'1-3'=>'1pm and 3pm',
'3-5'=>'3pm and 5pm'

	);

if(isset($_POST['RequiredTime'])&&$_POST['RequiredTime']!==""){

$humantime = $translatetime[$_POST['RequiredTime']];
}

if($extrainfo == 'delivery'){

echo "<p><b>for</b> delivery on ".$humandate ."<br /><b>to</b> ".$_POST['RecipientName']." in ".$delivery_postcode.".</p>";
echo "<br /><p><b>Please note</b><span class=\"ordercost\">". $notices."</span></p>";

}else{

echo "<p><b>for</b>Pick up from our shop in Poynton<br /><b>on</b> ".$humandate."<br /><b>between</b> ".$humantime.".</p>";

}

echo "</div>";

// now we build up the form for handing over.

include("_cardsavehashes.php");

?>
<form name="contactForm" id="contactForm" method="post" action="https://mms.cardsaveonlinepayments.com/Pages/PublicPages/PaymentForm.aspx" target="_self">
		
<?php $transactiontime = date('Y-m-d H:i:s P'); ?>
	<input type="hidden" name="HashDigest" value="<?php createhash($orderID, $charge_customer, $CallbackURL, $transactiontime); ?>" />
				<input type="hidden" name="MerchantID" value="GreenE-2568265" />
				<input type="hidden" name="Amount" value="<?php echo $charge_customer; ?>" />
				<input type="hidden" name="CurrencyCode" value="826" />
				<input type="hidden" name="OrderID" value="<?php echo $orderID; ?>" />
				<input type="hidden" name="TransactionType" value="SALE" />
				<input type="hidden" name="TransactionDateTime" value="<?php echo $transactiontime; ?>" />
				<input type="hidden" name="CallbackURL" value="<?php echo $CallbackURL; ?>" />
				<input type="hidden" name="OrderDescription" value="Flowers from Green Earth Flowers" />
				<input type="hidden" name="CustomerName" value="" />
				<input type="hidden" name="Address1" value="" />
				<input type="hidden" name="Address2" value="" />
				<input type="hidden" name="Address3" value="" />
				<input type="hidden" name="Address4" value="" />
				<input type="hidden" name="City" value="" /> 
				<input type="hidden" name="State" value="" />
				<input type="hidden" name="PostCode" value="" />
				<input type="hidden" name="CountryCode" value="826" />
				<input type="hidden" name="CV2Mandatory" value="true" />
				<input type="hidden" name="Address1Mandatory" value="true" />
				<input type="hidden" name="CityMandatory" value="true" />
				<input type="hidden" name="PostCodeMandatory" value="true" />
				<input type="hidden" name="StateMandatory" value="true" />
				<input type="hidden" name="CountryMandatory" value="true" />
				<input type="hidden" name="ResultDeliveryMethod" value="POST" />
				<input type="hidden" name="ServerResultURL" value="" />
				<input type="hidden" name="PaymentFormDisplaysResult" value="false" />




<input type="submit" value="Pay for this order" />
</form>


<div class="cardsave sendover">
<img src="buy_images/cardsave.png" alt="supported cards" />
</div>

