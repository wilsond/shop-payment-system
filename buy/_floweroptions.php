<?php

// this array for
// name of category in square boxes
// intro image
// selector for array with actual info in it
// description for category

$flower_categories = Array(

Array(

'category'=>'Exotic Bouquet',
'cat_selector'=>'exotic',
'cat_description'=>'<p>A dramatic and colourful handtied bouquet of exotic flowers and tropical foliages.</p><p>This bold and bright bouquet will be arranged by our florists using a selection of the day&#8217;s freshest blooms, and will typically include heliconias, orchids, anthuriums, proteas and a selection of lush tropical leaves.</p><p>All bouquets are beautifully gift wrapped and delivered in water.</p>',
'cat_optionsandprices'=>'30|standard,40|luxury,50|splendid,75|magnificent',
'cat_imagerepresent'=>'Image is representative of a &pound;50 bouquet.'

),

Array(

'category'=>'Seasonal Bouquet',
'cat_selector'=>'seasonal',
'cat_description'=>'<p>A rustic handtied bouquet of flowers and foliages that reflect the season.</p><p> Leave it to us to choose a selection from the day&#8217;s freshest blooms, or we can design a bouquet around a favourite flower or colour.</p><p>All bouquets are beautifully gift wrapped and delivered in water.</p>',
'cat_optionsandprices'=>'30|standard,40|luxury,50|splendid,75|magnificent',
'cat_imagerepresent'=>'Image is representative of a &pound;40 bouquet.'

),

Array(

'category'=>'Classic Bouquet',
'cat_selector'=>'classic',
'cat_description'=>'<p>A stylish handtied bouquet in a classic white and green colour scheme.</p><p>Leave it to us to choose a selection from the day&#8217;s freshest flowers and foliages, or let us know their favourite flower and we will do our best to include it (depending on seasonal availability).</p><p>All bouquets are beautifully gift wrapped and delivered in water.</p>',
'cat_optionsandprices'=>'30|standard,40|luxury,50|splendid,75|magnificent',
'cat_imagerepresent'=>'Image is representative of a &pound;40 bouquet.'

)

);



?>