$(document).ready(function() {

// hide anything that doesn't make sense if JS is on

$('.no-js').hide();

// validate inline

$('form').validate({

validClass: "success",
rules: {
RequiredDate:'required'
}
});

// highlight returned errors

$('span.errorhighlight').each(function(){

var mark = $(this).html();

//form *[name="FlowerOption"] {border:1px solid #c30 !important;}

$('form *[name="'+mark+'"]').css("borderColor","#fc0e0e");

});



// choose some flowers


$('.flowerchoice .options').hide();

$('.flowerchoice .options').addClass('noborders');

var visibleoption  = 0; // for initial state

// choose one type of flower

$('.chooser').click(function(){

if(visibleoption==0){

var target = $(this).attr('id').replace("_chooser","");

$('.options#'+target).slideDown('300');

$('html, body').animate({
        scrollTop: ($(".chooser").offset().top)+120
    }, 300);

$(this).addClass('selected').siblings().addClass('notselected');

visibleoption = 1; //only do this once

}else{

if($(this).hasClass('selected')){return false;} // clicked the same button


var tobehidden = $('.chooser.selected').attr('id');

console.log(tobehidden);

tobehidden = tobehidden.replace("_chooser","");

var tobeshown = $(this).attr('id').replace("_chooser","");

console.log(tobehidden+" "+tobeshown);

$('.options#'+tobehidden).slideUp('100', function(){

$('.chooser.selected').removeClass('selected');

$('.chooser.notselected').removeClass('notselected');


$('.chooser#'+tobeshown+"_chooser").addClass('selected').siblings().addClass('notselected');

$('.options#'+tobeshown).slideDown('300');

$('html, body').animate({
        scrollTop:  ($(".chooser").offset().top)+120
    }, 300);




$('input[name="FlowerOption"]').prop('checked', false);

$('label.js-chooseflower em').removeClass('sold');


});

}

return false;

});

// click on a buy link

// make buy buttons work (reset buy buttons on show/hide sections)


$('input[name="FlowerOption"]').addClass('invisible');

$('label.js-chooseflower').click(function(){

$('input[name="FlowerOption"]').prop('checked', false);

$('label.js-chooseflower em').removeClass('sold');

$(this).find('em').addClass('sold');

var checkradio  = $(this).attr('for');

$('input[value="'+checkradio+'"]').prop('checked',true);


continuefillingin();


return false;


})

// once we have chosen some flowers, what next?


function continuefillingin(){

// is this a first time click?

if($('.intro').is(':hidden')){

$('.intro').slideToggle('300', function(){

$('html, body').animate({
        scrollTop:  ($(".intro").offset().top)-120
    }, 300);


});



$('select.pod').addClass('invisible');

$("select.pod > option").each(function(){

var addtodiv = $(this).attr('value');

console.log(addtodiv);

if(this.value!==""){

var changetext = this.text;

$(".intro").append('<div class="fakepbuttons"><a class="'+this.value+'" href="#'+this.value+'">'+changetext+'</a></div>');

}

});


$('.fakepbuttons a').on('click', function(){

$('.fakepbuttons a').off('click');


var whichclick = $(this).attr('href').substring(1);

console.log(whichclick);

var whichdropdown = 'Fulfillment';

console.log(whichdropdown);

// IE7 passed the full url as attr href
//var whichvalue = $(this).attr('href');
//whichvalue = whichvalue.substring(1);

whichvalue = $(this).attr('href').split("#");

whichvalue = whichvalue[1];


// act on the actual select html

$('select[name="'+whichdropdown+'"] option').each(function(){

console.log("bbbb"+$(this).attr('value'));

if($(this).attr('value')== whichvalue){

$(this).attr("selected", "selected");

}

});

// BUTTON LOGIC IS HERE

if(whichclick == "pickup"){

$(this).parent().parent().addClass('selected');


$('fieldset.pickup').first().addClass('importantquestion').slideToggle('300');

//$('.flowerchoice').slideToggle('300');

//$('.pickupinstructions').slideToggle('300');

$('select#required_date option').eq(0).text("\u2014 Please choose a date to pick up \u2014");
$('select#required_time option').eq(0).text("\u2014 then choose a pick up timeslot \u2014");

// remove last option for Wednesdays, remove 5pm on Saturday

$('select#required_date').on('change',function(){


console.log( $('select#required_date').children(':selected').text());

var testday = $('select#required_date').children(':selected').text().split(',');

// remove "selected" from any options that might already be selected
$("#required_time option:selected").prop("selected", false);
$("#required_time option:first").prop("selected", "selected");

if(testday[0]=="Wednesday"){

console.log('remove');

$("#required_time option:last").remove();

}else{

if($("#required_time option").length == 4){ 

$("select#required_time option:last").after('<option value="3-5">3pm &#8211; closing time</option>');

console.log('put back');

}
}

});



$('select#required_time').after('<input id="continue" type="submit" value="continue" />');




$('input#continue').click(function(){

    $(this).hide();

console.log($('select#required_date option:selected').val());

if($('select#required_date option:selected').val()!=="" && $('select#required_time option:selected').val()!=="" ){



var openup = $('select.pod').find(":selected").attr('value');

console.log(openup);

$('fieldset.'+openup).each(function(){

if($(this).is(':hidden')){$(this).slideToggle('300');}

});

$('.writemessage').hide();

$('textarea#rmessage').val('no message required');

$('fieldset.finishing h3').after('<div class="nomessage"><h3>Add a message?</h3><p>You can choose to write your own message on one of our message cards when you come to pick up the bouquet, or you can <a id="addnow" href="#addnow">add a message now</a> so the bouquet&#8217;s all ready to go.</p></div>');

$('a#addnow').click(function(){

$('.nomessage').hide();

$('textarea#rmessage').val('');

$('.writemessage').slideToggle('300');
return false;

});


}

return false;

});


}

if(whichclick == "delivery"){

  $(this).parent().parent().addClass('selected');

testdelivery(); // logic for rest of delivery is in the function

}



$('.fakepbuttons a').addClass('inactive');

$(this).removeClass('inactive');

$(this).addClass('clicked');

var getdecorator = $(this).text().toLowerCase();

$(this).contents().unwrap().parent().addClass('button '+getdecorator);


return false;

});


} // finish check for hidden


// finish continue filling in

}




// test the delivery area by ajax

function openuptime(){

$('.aftercheck').show();
   

$('fieldset.pickup').first().addClass('importantquestion').slideToggle('300');

//$('.flowerchoice').slideToggle('300');

//$('.pickupinstructions').slideToggle('300');

$('select#required_date option').eq(0).text("\u2014 Please choose a delivery date \u2014");
$('select#required_time').hide();


$('select#required_date').on('change', function(){

console.log($('select#required_date option:selected').val());

if($('select#required_date option:selected').val()!=="" ){


var openup = $('select.pod').find(":selected").attr('value');

console.log(openup);

//alert(openup);

$('fieldset.'+openup).each(function(){

if($(this).is(':hidden')){$(this).slideToggle('300');
//alert('opening');

}

});

}

});

}


function testdelivery(){

$('.postcodeask').clone().appendTo('.intro').addClass('temp');

$('.postcodeask.temp').prepend('<h3>Please enter the delivery postcode so we can check it is in our delivery area:</h3>');

$('.postcodeask.temp').append('<input id="checking" type="submit" value="check" />');

$('.postcodeask.temp').find('label').remove();

$('input#checking').removeAttr('disabled');
    
    $('input#checking').on('click',function(e){
      e.preventDefault();
    
    var buttonclicked = this;
    
    $(buttonclicked).attr('disabled','disabled');

var checkpostcode = $('select[name="DeliveryPostcode1"]').val()+ "-" + $('input[name="DeliveryPostcode2"]').val();

var duplicate1 =  $('select[name="DeliveryPostcode1"]').val().toUpperCase();

var duplicate2 =  $('input[name="DeliveryPostcode2"]').val().toUpperCase();

var humanpostcode = $('select[name="DeliveryPostcode1"]').val()+ " " + $('input[name="DeliveryPostcode2"]').val();
    
humanpostcode = humanpostcode.toUpperCase();

checkpostcode = checkpostcode.toUpperCase();


console.log(checkpostcode);

$.get( "_postcodechecker_ajax.php", { pc: checkpostcode } ).done(function( data ) {

var result = data;

$('p.result').remove();

if(result>0){

if(result == 1){

$('.postcodeask.temp').remove();
$('input#checking').remove();


$('.postcodeask select[name="DeliveryPostcode1"]').val(duplicate1);

$('.postcodeask input[name="DeliveryPostcode2"]').val(duplicate2);

$('.intro').after('<p class="result good">We can deliver to this postcode: <strong>there is a &pound;5 delivery charge</strong>.</p>');

$('input[name="DeliveryPostcode2"]').val(duplicate2);

$('input[name="DeliveryPostcode2"]').attr('readonly','readonly');

$('select[name="DeliveryPostcode1"]').attr('disabled', 'disabled');

openuptime();

}

if(result==2){

$('.postcodeask.temp').remove();
$('input#checking').remove();

$('.postcodeask input[name="DeliveryPostcode2"]').val(duplicate2);


$('.intro').after('<p class="result good">We can deliver to this postcode: <strong>there is no delivery charge</strong>.</p>');
$('input[name="DeliveryPostcode2"]').val(duplicate2);

$('input[name="DeliveryPostcode2"]').attr('readonly','readonly');

$('select[name="DeliveryPostcode1"]').attr('disabled', 'disabled');

openuptime();
}


}else{
$('#checking').after('<p class="result bad">Sorry, that doesn&#8217;t seem to be in our delivery area.<br />Please try again, or <a href="../contact/">contact the shop</a> for more information.</p>');

$(buttonclicked).attr('value','try again');

    $(buttonclicked).removeAttr('disabled');

return false;
}

    });
    
    return false;
  });


}

// turn the selects back on again

$('form').bind('submit', function() {
    
        $(this).find('select').removeAttr('disabled');
    });

});