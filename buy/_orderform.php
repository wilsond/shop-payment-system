
<div class="main">
<div class="header">

	<div class="progress">
<ul>
<li><b>1</b> Order information</li>
<li class="todo"><b>2</b> Payment information and checkout</li>
<li class="todo"><b>3</b> Done!</li>
</ul>
</div>


<h2>Order a bouquet<!-- for local delivery or pick up from the shop --></h2>

<div class="instructions">

	<p>We deliver from Monday to Saturday within the local area. Delivery is free in Poynton, and there is a &pound;5 delivery charge everywhere else.</p>
<p>
	Alternatively, you can choose to pick up your bouquet from the shop in Poynton.
</p>

<p>If you need more than one bouquet delivered, or you&#8217;d like to discuss wedding flowers or sympathy flowers, please <a href="../contact/">contact the shop</a>.</p>
</div>


</div>
<?php echo @$errorprintout.@$jqueryerrorpickup; 

//print_r($_POST); 
//echo $orderID; 

?>




<form method="post" action="index.php">
		
		

<fieldset class="flowerchoice">
<h3 class="cyb">Choose your bouquet</h3>

<?php 

foreach($flower_categories as $fc){

?>

<div class="chooser" id="<?php echo $fc['cat_selector']; ?>_chooser">
<a href="#<?php echo $fc['cat_selector']; ?>">
<span class="bt"><?php echo str_replace("Bouquet", "", $fc['category']); ?></span>
<img src="buy_images/<?php echo $fc['cat_selector']; ?>_thumb.jpg" alt="Example of <?php echo $fc['category']; ?>" />
</a>
</div>

<?php

}


foreach($flower_categories as $fc){

?>

<div class="options" id="<?php echo $fc['cat_selector']; ?>">
<div class="image">
<img src="buy_images/<?php echo $fc['cat_selector']; ?>_detail.jpg" alt="Example of <?php echo $fc['category']; ?>" />
<p class="imageamount"><?php echo $fc['cat_imagerepresent']; ?></p>
</div>

<div class="about">
<h3><?php echo $fc['category']; ?></h3>

<?php echo $fc['cat_description']; ?>
</div>

<div class="buyoptions">
<ul>
<?php 

$prices = explode(',', $fc['cat_optionsandprices']);

foreach ($prices as $p){

$p = explode('|', $p);

echo '<li><label class="js-chooseflower" for="'.$fc['cat_selector'].'_'.$p[1].'_'.$p[0].'"><span>'.$p[1].'</span> <b>&pound;'.$p[0].'</b> <em>BUY</em></label>';



echo '<input name="FlowerOption" value="'.$fc['cat_selector'].'_'.$p[1].'_'.$p[0].'" id="'.$fc['cat_selector'].'_'.$p[1].'_'.$p[0].'" type="radio" />';

echo "</li>";

}

?>
</ul>
</div>

</div>
<?php
}
?>


</fieldset>


<fieldset class="intro no-js">

<h3>Pick up or delivery?</h3>





<select name="Fulfillment" class="pod">
<option value="">&#8212; Please choose an option &#8212;</option>
<option value="pickup" <?php if(isset($_POST['Fulfillment'])&&($_POST['Fulfillment']=="pickup")){ echo "selected=\"selected\""; } ?>>Pick up</option> 
<option value="delivery" <?php if(isset($_POST['Fulfillment'])&&($_POST['Fulfillment']=="delivery")){ echo "selected=\"selected\""; } ?>>Delivery</option>
</select>


</fieldset>


<fieldset class="no-js delivery pickup">

<h3>When do you need the flowers?</h3>


<p>
<select name="RequiredDate" id="required_date" class="required">
	<option value="">&#8212; Please choose a pickup or delivery date &#8212;</option>

<?php

$checktimeofday = localtime();
$checktimeofday = $checktimeofday[2];
// 13 = 1pm
if($checktimeofday<13){
$start = 0;
}else{
$start = 1;
}


$date = time();

for($i=$start; $i<36;$i++){

// have a list of excluded days

$excluded = array(
	'2013-12-25',
	'2013-12-26', 
	'2013-12-27', 
	'2013-12-28', 
	'2013-12-29', 
	'2013-12-30', 
	'2013-12-31', 
	'2013-01-01', 
	'2013-01-02', 
	'2013-01-03', 
	'2013-01-04', 
	'2013-01-05'
 

	);

$newDate = strtotime('+'.$i.' days',$date);
$checkdate = date('l, jS F',$newDate);
$backenddate = date('Y-m-d', $newDate);

if(strstr($checkdate,"Sunday")){

}else{
if($i==0){$today = "Today: ";}else{$today ="";}

$addselected ='';

if(isset($_POST['RequiredDate'])&&($_POST['RequiredDate']!=="")){

if($_POST['RequiredDate']==$backenddate){$addselected = "selected= \"selected\""; }

}

if(in_array($backenddate, $excluded))

	{
	// closed
	}else{

echo '<option value="'.$backenddate.'" '.$addselected.'>'.$today.$checkdate.'</option>';
}
}

}
?>

</select>
</p>
<p>
<select name="RequiredTime" id="required_time">
	<option value="">&#8212; If you chose pick up, please choose a time slot &#8212;</option>

<?php 




$deliveryoptions = array(

array('backend'=>"9-11", 'human'=>"9am &#8211; 11am"),
array('backend'=>"11-1", 'human'=>"11am &#8211; 1pm"),
array('backend'=>"1-3", 'human'=>"1pm &#8211; 3pm"),
array('backend'=>"3-5", 'human'=>"3pm &#8211; closing time"),

	);

foreach ($deliveryoptions as $d){

$addselected ='';

if(isset($_POST['RequiredTime'])&&($_POST['RequiredTime']!=="")){

if($_POST['RequiredTime']==$d['backend']){$addselected = "selected= \"selected\""; }

}

echo '<option value="'.$d['backend'].'" '.$addselected.'>'.$d['human'].'</option>';

}

?>

</select>
</p>


</fieldset>




<fieldset class="no-js delivery">



<h3>About the recipient</h3>



<label for="rname">Name</label>
<input type="text" name="RecipientName" class="required" id="rname" size="50" value="<?php echo @$_POST['RecipientName']; ?>" />

<label for="rphone">Contact phone number</label>
<input type="text" id="rphone" size="20" class="required" name="RecipientPhone" value="<?php echo @$_POST['RecipientPhone']; ?>" />


<label for="delivery_address1">Delivery address <span class="no-js">if applicable</span></label>
<p><input type="text" name="DeliveryAddress1" class="required" size="50" id="delivery_address1" value="<?php echo @$_POST['DeliveryAddress1']; ?>" /></p>
<p>
<input type="text" name="DeliveryAddress2" class="required" size="50" id="delivery_address2" value="<?php echo @$_POST['DeliveryAddress2']; ?>" />
</p>
<p>
<input type="text" name="DeliveryAddress3" size="50" id="delivery_address3" value="<?php echo @$_POST['DeliveryAddress3']; ?>" />
</p>
<p class="help">The last line of the address is optional.</p>

<div class="postcodeask">
<label for="DeliveryPostcode2">Delivery postcode <span class="no-js">if applicable</span></label>

<select name="DeliveryPostcode1">

<?php

$postcode_options = Array(
	'SK1',
	'SK2',
	'SK3',
	'SK6',
	'SK7',
	'SK8',
	'SK9',
	'SK10',
	'SK11',
	'SK12',
	'SK22'
	);

foreach($postcode_options as $po){

$selectoption = '';

if(isset($_POST['DeliveryPostcode1'])&&$_POST['DeliveryPostcode1']!==''){

if($po == $_POST['DeliveryPostcode1']){ $selectoption = "selected = \"selected\""; }

}else{

if($po == 'SK12'){ $selectoption = "selected = \"selected\""; }

}

?>
<option value="<?php echo $po; ?>" <?php echo $selectoption; ?>><?php echo $po; ?></option>

<?php } ?>





</select>

<input name="DeliveryPostcode2" id="DeliveryPostcode2"type="text" size="4" class="required" value="<?php echo @$_POST['DeliveryPostcode2']; ?>" />

</div>

<label for="delivery_instruction">Any special delivery instructions?</label>
<textarea id="delivery_instruction" name="DeliveryInstructions" rows="2" cols="48"><?php echo @$_POST['DeliveryInstructions']; ?></textarea>
<p class="help">This question is optional.</p>


</fieldset>


<fieldset class="no-js delivery pickup finishing">
	<h3>Finishing touches</h3>

<div class="writemessage">
<label for="rmessage">Your message for the recipient</label>
<textarea id="rmessage" name="RecipientMessage" rows="4" cols="48"><?php echo @$_POST['RecipientMessage']; ?></textarea>
</div>

	


<label for="special">Any special flower or colour requests for this bouquet?</label>
<textarea id="special" name="SpecialRequests" rows="2" cols="48"><?php echo @$_POST['SpecialRequests']; ?></textarea>

<p class="help">This box is optional.</p>




</fieldset>





<fieldset class="no-js delivery pickup">


<h3>Your contact details</h3>

<label for="order_name">Your name</label>
<input type="text" name="CustomerName" class="required" id="order_name" size="50" value="<?php echo @$_POST['CustomerName']; ?>" />

<label for="order_phone">Contact phone number</label>
<input type="text" name="OrderPhone" class="required" id="order_phone" size="20" value="<?php echo @$_POST['OrderPhone']; ?>" />


<label for="order_email">E-mail address</label>
<input type="text" name="OrderEmail" class="required email" id="order_email" size="30" value="<?php echo @$_POST['OrderEmail']; ?>"/>

<p class="help">We need your phone number and email address in case we need to contact you about this order.</p>


				





<input type="submit" value="Continue" name="formsubmit" />
<p class="help">You can review your order before you continue to the payment page.</p>
</fieldset>

</form>

<div class="cardsave">
<img src="buy_images/cardsave.png" alt="supported cards" />
</div>

</div>