<?php


function whatdidtheyorder($id){

echo "<hr />".$id;

}

function internalmail( $subject, $message ){
	
$headers = 'From: buyonline@greenearthflowers.com' . "\r\n" .
    'Reply-To: buyonline@greenearthflowers.com' . "\r\n" .
    'X-Mailer: PHP/' . phpversion(). "\r\n" .
    'Content-Type: text/plain; charset=UTF-8';


mail('d@derrenwilson.net', $subject, $message, $headers);


}

function externalmail($customerdetails){

$headers = 'From: buyonline@greenearthflowers.com' . "\r\n" .
    'Reply-To: buyonline@greenearthflowers.com' . "\r\n" .
    'X-Mailer: PHP/' . phpversion(). "\r\n" .
	'Content-Type: text/plain; charset=UTF-8';

require_once('_addrecordtodatabase.php');

$alldetails = retrievetransaction($customerdetails);



$alldetails['RequiredDate'] = date('l, jS F Y', strtotime($alldetails['RequiredDate']));

$fo = explode("_", $alldetails['FlowerOption']);

$flower_detail = "a £".$fo[2]." ".ucfirst($fo[0]).' bouquet';

if($alldetails['Fulfillment'] =='delivery'){

$deliveryaddress = $alldetails['DeliveryAddress1']."\r\n".
$alldetails['DeliveryAddress2']."\r\n".
$alldetails['DeliveryAddress3']."\r\n".
$alldetails['DeliveryPostcode1'].' '.strtoupper($alldetails['DeliveryPostcode2']);

$content = 'Dear '.$alldetails['CustomerName']."\r\n"."\r\n"."ORDER REFERENCE: gef_00".$alldetails['OrderID']."\r\n"."\r\n"."Thank you for placing an order with Green Earth Flowers."."\r\n"."\r\n"."Just to confirm, you have ordered:"."\r\n\r\n".$flower_detail."\r\n"."to be delivered on ".$alldetails['RequiredDate']."\r\n"."to ".$alldetails['RecipientName']."\r\n"."at"."\r\n".$deliveryaddress."."."\r\n"."\r\n"."If you need to make any changes to this order,\r\nplease contact the shop on tel:01625859525\r\nor email mailto:shop@greenearthflowers."."\r\n"."\r\n"."Many thanks for your order and we hope to see you again soon."."\r\n"."\r\n"."Green Earth Flowers";


}

if($alldetails['Fulfillment']=='pickup'){


$do = array(

"9-11"=>"9am - 11am",
"11-1"=>"11am - 1pm",
"1-3"=>"1pm - 3pm",
"3-5"=>"3pm - closing time"

	);

	$content = "Dear ".$alldetails['CustomerName']."\r\n\r\nORDER REFERENCE: gef_00".$alldetails['OrderID']."\r\n\r\nThankyou for placing an order with Green Earth Flowers.\r\n\r\nJust to confirm, you have ordered ".$flower_detail."\r\nto be collected from the shop\r\non ".$alldetails['RequiredDate']."\r\nbetween ".$do[$alldetails['RequiredTime']].".\r\n\r\nIf you haven't visited us before, you can find directions here: http://bit.ly/18bX1to.\r\n\r\nIf you need to make any changes to this order,\r\nplease contact the shop on tel:01625 859525\r\nor email mailto:shop@greenearthflowers.\r\n\r\nMany thanks for your order and we hope to see you again soon.\r\n\r\nGreen Earth Flowers";


}


mail($alldetails['OrderEmail'], 'Your flower order from Green Earth Flowers', $content, $headers);

}


?>
