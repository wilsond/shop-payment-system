<?php include("../includes/build.php"); 


pageheader('About Us', 'about');

pagenav('about');


?>

<div class="content">
<p>Green Earth Flowers was established in 2006 by Laura Taylor. In 2003, following ten years in London and a successful career working for a contemporary furniture design company, she decided to go back to college to study floristry. 
</p>
<p>
After gaining experience in other florist shops, the opportunity to take the plunge and open her own business came along at the end of 2006. The aim was always to be a little different, to offer top quality flowers, contemporary designs and the very best customer service.</p>
<p class="rest">Over the last 5 years, Green Earth Flowers has developed an enviable reputation in the local area, particularly for their stunning wedding flowers and friendly, personal service. The shop has been included in the Good Florist Guide for the last 3 years. Laura is supported by a team of highly-trained, creative florists, who share her philosophy to provide the very best. </p>
</div>


<?

pagefooter('stretch,gef', 'i/bgimages/aboutus.jpg');


?>

