<?php 

include("includes/build.php"); 

pageheader('Error', 'error');

pagenav('error');

?>
</div>

<div class="content error">
<p class="errormessage">We couldn&#8217;t find what you were looking for.</p>
<p>The page may have moved or you may have mistyped the address.</p>

<?php

pagefooter('stretch,gef', 'i/bgimages/homepage.jpg');

?>

