<?php 

$sympathyhtml="<div class=\"sympathyflowers\">";

for ($i=1; $i<15; $i++){ 
$sympathyhtml.="<img src=\"../i/funerals/$i.jpg\" alt=\"funeral image\" />";
 } 
 
 $sympathyhtml.="</div>";
 
 ?>

<?php include("../includes/build.php"); 


pageheader('Sympathy Flowers', 'sympathy');

pagenav('sympathy');


?>


<div class="content">
<p>
At Green Earth Flowers we understand the importance of choosing meaningful flowers as a final tribute to a friend or loved one.</p><p>From delicate posies to elaborate coffin sprays, we can advise you on the most appropriate form of tribute, or design something unique to reflect the personality of the deceased.
</p>

<p class="asbutton symp"><a onClick="_gaq.push(['_trackEvent', 'sympathy brochure', 'download', 'v1']);" href="gef_funerals.pdf">Download a Sympathy Brochure <i>PDF</i></a></p>

<p>
Please call us on 01625 859525 or <a href="../contact/">visit the shop</a> for more information.</p>
</div>



<?

pagefooter('stretch,gef', 'i/bgimages/sympathy.jpg');

//pagefooter('gef');

?>

