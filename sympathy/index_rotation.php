<?php 


$navigationpage = '<p><a id="nextslide" class="load-item"></a></p>';

include("../includes/build.php"); 


pageheader('Sympathy Flowers', 'sympathy',$navigationpage);

pagenav('sympathy');


?>


<div class="content">
<p>
At Green Earth Flowers we understand the importance of choosing meaningful flowers as a final tribute to a friend or loved one.</p><p>From delicate posies to elaborate coffin sprays, we can advise you on the most appropriate form of tribute, or design something unique to reflect the personality of the deceased.
</p><p>
Please call us on 01625 859525 or <a href="../contact/">visit the shop</a> for more information.</p>
</div>



<?

pagefooter('gef,sg', 'i/bgimages/sympathy.jpg');

//pagefooter('gef');

?>

