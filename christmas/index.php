<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Christmas flowers &middot; Green Earth Flowers, your local florist in Poynton, Cheshire</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="Content-Language" content="en-gb" />
<meta name="description" content="Welcome to Green Earth Flowers, your local florist in Poynton, Cheshire, delivering to South Manchester" />
<meta name="keywords" content="delivery, local, weddings, funeral, sympathy, gifts, cards" />

<meta name="author" content="Derren Wilson" />
<meta name="sponsor" content="Green Earth Flowers" />
<meta name="copyright" content="Green Earth Flowers" />
<meta http-equiv="imagetoolbar" content="false" />
<meta name="MSSmartTagsPreventParsing" content="true" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="viewport" content="width=device-width" />


<link rel="Shortcut Icon" href="/favicon.ico" type="image/x-icon" />


<link href="http://fnt.webink.com/wfs/webink.css/?project=CA2E9E8A-8DA0-45EE-8C0D-32E817C57D4B&amp;fonts=49E90CB6-0944-72CE-A0E7-8D521C8E676C:f=AdelleSans-Light,0EFF2364-9D58-0735-E88C-990B2756000B:f=AdelleSans-Bold" rel="stylesheet" type="text/css"/>


<link rel="stylesheet" href="../css/g.css" type="text/css" />
<link rel="stylesheet" href="../css/gshop.css" type="text/css" />

		
<!-- <script type="text/javascript" src="/slimstat/?js"></script> -->
<script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-2231826-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
  </script>
</head>
<body id="buy"><div id="c"><div class="pc"><div class="nav"><h1><a href="/">Green Earth Flowers: Weddings and Flowers in Poynton, Cheshire</a></h1><ul><li><a href="../buy/index.php">Buy online</a></li><li><a href="../weddings/index.php">Weddings</a></li><li><a href="../deliveries/index.php">Deliveries</a></li></ul><ul><li><a href="../sympathy/index.php">Sympathy</a></li><li><a href="../contact/index.php">Contact Us</a></li><li><a href="../about/index.php">About Us</a></li></ul>


<div class="special"><a href="christmas/">Christmas</a></div>

</div></div><div class="footer"><p class="big">01625 859525 &middot; <a href="#c">top</a></p>
<p>
&copy; 2013 <a href="mailto:shop@greenearthflowers.com">Green Earth Flowers</a></p></div>
<div class="main">

<div class="xmas">
	<div class="bt">
	<h2>
Festive</h2>
<div class="description">
<p>
A seasonal hand-tied bouquet in rich shades of red, burgundy and purple,
including velvety amaryllis and ilex berries.
</p>
<ul><li><span>
Standard</span> &pound;35</li>
<li><span>Luxury</span> &pound;45</li>
<li><span>Splendid</span> &pound;60</li><li><span>Magnificent</span> &pound;75</li></ul>
</div>
<div class="image">
<img src="../buy/buy_images/festive_detail.jpg" alt="Festive bouquet" />
	</div>

</div>
<div class="bt">
<h2>Wintry</h2>
<div class="description">

<p>
A rustic hand-tied bouquet in frosty whites and creams with silvery foliages
and berries.</p>
<ul><li><span>Standard</span> &pound;30</li><li><span>Luxury</span> &pound;40</li><li><span>Splendid</span> &pound;50</li><li><span>Magnificent</span> &pound;75</li></ul>
</div>

<div class="image">
<img src="../buy/buy_images/wintry_detail.jpg" alt="Wintry bouquet"  />
	</div>


</div>
<div class="bt">
<h2>
Contemporary</h2>
<div class="description">

<p>
A stylish grouped hand-tied bouquet in warm tones of peach, cream and burnt
orange, including gerberas and anthuriums.</p>
<ul><li><span>Standard</span> &pound;25</li><li><span>Luxury</span> &pound;35</li><li><span>Splendid</span> &pound;45</li><li><span>Magnificent</span> &pound;60</li></ul>
</div>
<div class="image">
<img src="../buy/buy_images/contemporary_detail.jpg" alt="Contemporary bouquet" />
	</div>

</div>
<div class="bt">
<h2>Table Centres and<br />Festive Arrangements</h2>
<div class="description">

<p>
Choose from a selection of ready-made designs in various containers or have one made to order.</p>
<ul>
	<li>
Standard &pound;20 &#8211; &pound;35</li>
<li>
Luxury &pound;35 &#8211; &pound;50
</li></ul>
</div>
</div>

</div></div>
</body>
</html>



