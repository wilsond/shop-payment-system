<?php include("../includes/build.php"); 


pageheader('Contact Us', 'contact');

pagenav('contact');


?>

<div class="content">

<div class="opening">
<p class="phone"><i>Phone</i> <b>01625&nbsp;859525</b></p>

</div>
<div class="group hours">
<p>Monday &#8211; Friday <b>9am &#8211; 5pm</b></p>
<p>Wednesday <b>9am &#8211; 3pm</b></p>

<p>Saturday <b>9am &#8211; 4pm</b></p>
<p>Sunday <b>closed</b></p>
</div>

<div class="group address">
<p>40 Park Lane</p>
<p>Poynton, Cheshire SK12 1RE</p>
<p><a  class="icon google" href="http://maps.google.co.uk/maps?q=Green+Earth+Flowers,+Park+Lane,+Poynton&amp;hl=en&amp;sll=53.348643,-2.12034&amp;sspn=0.001465,0.003028&amp;oq=green+eart&amp;t=h&amp;hq=Green+Earth+Flowers,&amp;hnear=Park+Ln,+Poynton,+United+Kingdom&amp;z=16"><i>Find us on google maps</i></a></p>

</div>

<div class="group social">
<p><a class="icon fb" href="http://www.facebook.com/greenearthflowers"><i>Like us on facebook</i></a></p>
<p><a class="icon tw" href="http://twitter.com/greenearthflwrs"><i>Follow us on twitter</i></a></p>
</div>

</div>

<div class="form">
<form action="../mail.php" method="post">
<p>
<label>Name</label>
<input type="text" name="sendername" size="40" class="required" minlength="2" />
</p>

<p>
<label>Email address</label>
<input type="text" name="senderemail" size="40" class="required email contactgroup" />
</p>

<p>
<label>Phone number</label>
<input type="text" name="senderphone" size="40" class="required contactgroup" minlength="2" />
</p>

<p>
<label>Your message</label>
<textarea rows="5" name="sendermessage" cols="40" class="required"></textarea>
</p>

<p>
<button type="submit">Send</button>
</p>
</form>

</div>


<?

//pagefooter('stretch,gef', 'i/bgimages/contactus.jpg');

pagefooter('stretch,form,gef', 'i/bgimages/contactus.jpg');


?>

