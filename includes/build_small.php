<?php

$root = "/";


function pageheader($title, $id, $prehtml=""){

global $root;

echo '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>'.$title.' &middot; Green Earth Flowers, your local florist in Poynton, Cheshire</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="Content-Language" content="en-gb" />
<meta name="description" content="Welcome to Green Earth Flowers, your local florist in Poynton, Cheshire, delivering to South Manchester" />
<meta name="keywords" content="delivery, local, weddings, funeral, sympathy, gifts, cards" />

<meta name="author" content="Derren Wilson" />
<meta name="sponsor" content="Green Earth Flowers" />
<meta name="copyright" content="Green Earth Flowers" />
<meta http-equiv="imagetoolbar" content="false" />
<meta name="MSSmartTagsPreventParsing" content="true" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="viewport" content="width=device-width" />


<link rel="Shortcut Icon" href="/favicon.ico" type="image/x-icon" />


<link href="http://fnt.webink.com/wfs/webink.css?project=CA2E9E8A-8DA0-45EE-8C0D-32E817C57D4B&amp;fonts=49E90CB6-0944-72CE-A0E7-8D521C8E676C:family=AdelleSans-Light,29AEAE82-9A63-C919-5496-92FEA65ACA5E" rel="stylesheet" type="text/css"/>


<link rel="stylesheet" href="'.$root.'css/g.css" type="text/css" />
<link rel="stylesheet" href="'.$root.'css/g_small.css" type="text/css" />
<link rel="stylesheet" href="'.$root.'css/print.css" type="text/css" media="print"  />
<link rel="stylesheet" href="'.$root.'css/supersized.css" type="text/css" media="screen" />
		<link rel="stylesheet" href="'.$root.'css/supersized.shutter.css" type="text/css" media="screen" />
		
<script type="text/javascript" src="/slimstat/?js"></script>
<script type="text/javascript">';

echo <<<GOOG

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-2231826-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
  
GOOG;
  
echo '</script>
</head>
<body id="'.$id.'">'.$prehtml.'<div id="c">';


}

function pagenav($current_page){

global $root;

echo '<div class="pc"><div class="nav">';

if($current_page<>"homepage"){
echo '<h1><a href="'.$root.'">Green Earth Flowers: Weddings and Flowers in Poynton, Cheshire</a></h1>';
}else{
echo '<h1 class="nolink">Green Earth Flowers: Weddings and Flowers in Poynton, Cheshire</h1>';
}

$pages = Array(

//Array('url'=>'index.php', 'human'=>'Home'),

//Array('url'=>'special/index.php', 'human'=>'Mother&#8217;s Day', 'class'=>'special'),

Array('url'=>'weddings/index.php', 'human'=>'Weddings'),
Array('url'=>'deliveries/index.php', 'human'=>'Deliveries'),
Array('url'=>'sympathy/index.php', 'human'=>'Sympathy'),
Array('url'=>'contact/index.php', 'human'=>'Contact Us'),
Array('url'=>'about/index.php', 'human'=>'About Us')

);

echo '<ul>';

$i=0;

foreach($pages as $p){

if(strstr( $p['url'], $current_page)){

$aclass=' class="active" ';
}else{
$aclass='';
}

if(isset($p['class'])){

echo '<li class="'.$p['class'].'">';

}else{

echo '<li>';

}
echo '<a '.$aclass.'href="'.$root.$p['url'].'">'.$p['human'].'</a></li>';

$i++;

if($i==3){echo "</ul><ul>";}

}


echo '</ul>';

echo '</div>'; // end of navandcontent

}




function pagefooter($jquery="", $extravars="",$totalimages="",$startimage=""){

global $root;



echo '</div><div class="footer"><p class="big">01625 859525 &middot; <a href="#c">top</a></p>
<p>
&copy; '.date('Y').' <a href="mailto:shop@greenearthflowers.com">Green Earth Flowers</a></p></div></div>';

echo '<script src="'.$root.'js/ios-orientationchange-fix.js" type="text/javascript"></script>';

echo '</body>
</html>';



}

?>