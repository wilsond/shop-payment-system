<?php

$root = "http://localhost:8888/gef/";


function pageheader($title, $id, $prehtml=""){

global $root;

echo '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>'.$title.' &middot; Green Earth Flowers, your local florist in Poynton, Cheshire</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="Content-Language" content="en-gb" />
<meta name="description" content="Welcome to Green Earth Flowers, your local florist in Poynton, Cheshire, delivering to South Manchester" />
<meta name="keywords" content="delivery, local, weddings, funeral, sympathy, gifts, cards" />

<meta name="author" content="Derren Wilson" />
<meta name="sponsor" content="Green Earth Flowers" />
<meta name="copyright" content="Green Earth Flowers" />
<meta http-equiv="imagetoolbar" content="false" />
<meta name="MSSmartTagsPreventParsing" content="true" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="viewport" content="width=device-width" />


<link rel="Shortcut Icon" href="/favicon.ico" type="image/x-icon" />


<link href="http://fnt.webink.com/wfs/webink.css?project=CA2E9E8A-8DA0-45EE-8C0D-32E817C57D4B&amp;fonts=49E90CB6-0944-72CE-A0E7-8D521C8E676C:family=AdelleSans-Light,29AEAE82-9A63-C919-5496-92FEA65ACA5E" rel="stylesheet" type="text/css"/>


<link rel="stylesheet" href="'.$root.'css/g.css" type="text/css" />
<link rel="stylesheet" href="'.$root.'css/print.css" type="text/css" media="print"  />
<link rel="stylesheet" href="'.$root.'css/supersized.css" type="text/css" media="screen" />
		<link rel="stylesheet" href="'.$root.'css/supersized.shutter.css" type="text/css" media="screen" />
		
<script type="text/javascript" src="/slimstat/?js"></script>
<script type="text/javascript">';

echo <<<GOOG

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-2231826-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
  
GOOG;
  
echo '</script>
</head>
<body id="'.$id.'">'.$prehtml.'<div id="c">';


}

function pagenav($current_page){

global $root;

echo '<div class="pc"><div class="nav">';

if($current_page<>"homepage"){
echo '<h1><a href="'.$root.'">Green Earth Flowers: Weddings and Flowers in Poynton, Cheshire</a></h1>';
}else{
echo '<h1 class="nolink">Green Earth Flowers: Weddings and Flowers in Poynton, Cheshire</h1>';
}

$pages = Array(

//Array('url'=>'index.php', 'human'=>'Home'),

//Array('url'=>'special/index.php', 'human'=>'Christmas flowers', 'class'=>'special'),
Array('url'=>'buy/index.php', 'human'=>'Buy online'),

Array('url'=>'weddings/index.php', 'human'=>'Weddings'),
Array('url'=>'deliveries/index.php', 'human'=>'Deliveries'),
Array('url'=>'sympathy/index.php', 'human'=>'Sympathy'),
Array('url'=>'contact/index.php', 'human'=>'Contact Us'),
Array('url'=>'about/index.php', 'human'=>'About Us')

);

echo '<ul>';

$i=0;

foreach($pages as $p){

if(strstr( $p['url'], $current_page)){

$aclass=' class="active" ';
}else{
$aclass='';
}

if(isset($p['class'])){

echo '<li class="'.$p['class'].'">';

}else{

echo '<li>';

}
echo '<a '.$aclass.'href="'.$root.$p['url'].'">'.$p['human'].'</a></li>';

$i++;

if($i==3){echo "</ul><ul>";}

}


echo '</ul>';

echo '<div class="special"><a href="christmas/">Christmas</a></div>';

echo '</div>'; // end of navandcontent

}




function pagefooter($jquery="", $extravars="",$totalimages="",$startimage=""){

global $root;



echo '</div><div class="footer"><p class="big">01625 859525 &middot; <a href="#c">top</a></p>
<p>
&copy; '.date('Y').' <a href="mailto:shop@greenearthflowers.com">Green Earth Flowers</a></p></div></div>';

if($jquery<>""){

echo '<script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js" type="text/javascript"></script>';

if(strstr($jquery, "stretch")){

//http://srobbin.com/jquery-plugins/backstretch/

echo '<script src="'.$root.'js/jquery.backstretch.min.js" type="text/javascript"></script>
	<script type="text/javascript">
	    $.backstretch("'.$root.$extravars.'");
    </script>';


}

if(strstr($jquery, "gef")){
echo '<script src="'.$root.'js/gef.js" type="text/javascript"></script>';

}

if(strstr($jquery, "form")){
echo '<script src="'.$root.'js/jquery.validate.min.js" type="text/javascript"></script>';

}


if(strstr($jquery, "wg")){
echo '<script type="text/javascript" src="'.$root.'js/jquery.easing.min.js"></script>
		<script type="text/javascript" src="'.$root.'js/supersized.3.2.7.min.js"></script>
		<script type="text/javascript" src="'.$root.'js/supersized.shutter.min.js"></script>';

	echo '<script type="text/javascript">
			
			jQuery(function($){
				
				$.supersized({
				
					// Functionality
					slideshow               :   1,			// Slideshow on/off
					autoplay				:	0,			// Slideshow starts playing automatically
					start_slide             :   '.$startimage.',			// Start slide (0 is random)
					stop_loop				:	0,			// Pauses slideshow on last slide
					random					: 	0,			// Randomize slide order (Ignores start slide)
					slide_interval          :   4000,		// Length between transitions
					transition              :   6, 			// 0-None, 1-Fade, 2-Slide Top, 3-Slide Right, 4-Slide Bottom, 5-Slide Left, 6-Carousel Right, 7-Carousel Left
					transition_speed		:	800,		// Speed of transition
					new_window				:	0,			// Image links open in new window/tab
					pause_hover             :   0,			// Pause slideshow on hover
					keyboard_nav            :   1,			// Keyboard navigation on/off
					performance				:	1,			// 0-Normal, 1-Hybrid speed/quality, 2-Optimizes image quality, 3-Optimizes transition speed // (Only works for Firefox/IE, not Webkit)
					image_protect			:	1,			// Disables image dragging and right click with Javascript
															   
					// Size & Position						   
					min_width		        :   0,			// Min width allowed (in pixels)
					min_height		        :   0,			// Min height allowed (in pixels)
					vertical_center         :   1,			// Vertically center background
					horizontal_center       :   1,			// Horizontally center background
					fit_always				:	0,			// Image will never exceed browser width or height (Ignores min. dimensions)
					fit_portrait         	:   1,			// Portrait images will not exceed browser height
					fit_landscape			:   0,			// Landscape images will not exceed browser width
															   
					// Components							
					slide_links				:	false,	// Individual links for each slide (Options: false, \'num\', \'name\', \'blank\')
					thumb_links				:	0,			// Individual thumb links for each slide
					thumbnail_navigation    :   0,			// Thumbnail navigation
					slides 					:  	[			// Slideshow Images';
					
					
					
					

        
       
        
    

        for($i=1; $i<=$totalimages; $i++){

        	if($i==$totalimages){$codecomma="";}else{$codecomma=",";}

        echo "\n{image : '../i/weddings/".$_GET['w']."/".$i.".jpg', title : '', thumb : '', url : ''}$codecomma\n";
        

        }
    

 


					
					
													
					echo '],
												
					// Theme Options			   
					progress_bar			:	0,			// Timer for each slide							
					mouse_scrub				:	0
					
				});
		    });
		    
		</script>';


}



if(strstr($jquery, "sg")){


$images = 8;

echo '<script type="text/javascript" src="'.$root.'js/jquery.easing.min.js"></script>
		<script type="text/javascript" src="'.$root.'js/supersized.3.2.7.min.js"></script>
		<script type="text/javascript" src="'.$root.'js/supersized.shutter.min.js"></script>';

	echo '<script type="text/javascript">
			
			jQuery(function($){
				
				$.supersized({
				
					// Functionality
					slideshow               :   1,			// Slideshow on/off
					autoplay				:	0,			// Slideshow starts playing automatically
					start_slide             :   1,			// Start slide (0 is random)
					stop_loop				:	0,			// Pauses slideshow on last slide
					random					: 	0,			// Randomize slide order (Ignores start slide)
					slide_interval          :   4000,		// Length between transitions
					transition              :   6, 			// 0-None, 1-Fade, 2-Slide Top, 3-Slide Right, 4-Slide Bottom, 5-Slide Left, 6-Carousel Right, 7-Carousel Left
					transition_speed		:	800,		// Speed of transition
					new_window				:	0,			// Image links open in new window/tab
					pause_hover             :   0,			// Pause slideshow on hover
					keyboard_nav            :   1,			// Keyboard navigation on/off
					performance				:	1,			// 0-Normal, 1-Hybrid speed/quality, 2-Optimizes image quality, 3-Optimizes transition speed // (Only works for Firefox/IE, not Webkit)
					image_protect			:	1,			// Disables image dragging and right click with Javascript
															   
					// Size & Position						   
					min_width		        :   0,			// Min width allowed (in pixels)
					min_height		        :   0,			// Min height allowed (in pixels)
					vertical_center         :   1,			// Vertically center background
					horizontal_center       :   1,			// Horizontally center background
					fit_always				:	0,			// Image will never exceed browser width or height (Ignores min. dimensions)
					fit_portrait         	:   1,			// Portrait images will not exceed browser height
					fit_landscape			:   0,			// Landscape images will not exceed browser width
															   
					// Components							
					slide_links				:	false,	// Individual links for each slide (Options: false, \'num\', \'name\', \'blank\')
					thumb_links				:	0,			// Individual thumb links for each slide
					thumbnail_navigation    :   0,			// Thumbnail navigation
					slides 					:  	[			// Slideshow Images';
					
					
					
					

        
       
        
    

        for($i=1; $i<=$images; $i++){

        	if($i==$images){$codecomma="";}else{$codecomma=",";}

        echo "\n{image : '../i/sympathy/f".$i.".jpg', title : '', thumb : '', url : ''}$codecomma\n";
        

        }
    

 


					
					
													
					echo '],
												
					// Theme Options			   
					progress_bar			:	0,			// Timer for each slide							
					mouse_scrub				:	0
					
				});
		    });
		    
		</script>';


}




if(strstr($jquery, "veryspecial")){


$images = 2;

echo '<script type="text/javascript" src="'.$root.'js/jquery.easing.min.js"></script>
		<script type="text/javascript" src="'.$root.'js/supersized.3.2.7.min.js"></script>
		<script type="text/javascript" src="'.$root.'js/supersized.shutter.min.js"></script>';

	echo '<script type="text/javascript">
			
			jQuery(function($){
				
				$.supersized({
				
					// Functionality
					slideshow               :   1,			// Slideshow on/off
					autoplay				:	0,			// Slideshow starts playing automatically
					start_slide             :   1,			// Start slide (0 is random)
					stop_loop				:	0,			// Pauses slideshow on last slide
					random					: 	0,			// Randomize slide order (Ignores start slide)
					slide_interval          :   4000,		// Length between transitions
					transition              :   6, 			// 0-None, 1-Fade, 2-Slide Top, 3-Slide Right, 4-Slide Bottom, 5-Slide Left, 6-Carousel Right, 7-Carousel Left
					transition_speed		:	800,		// Speed of transition
					new_window				:	0,			// Image links open in new window/tab
					pause_hover             :   0,			// Pause slideshow on hover
					keyboard_nav            :   1,			// Keyboard navigation on/off
					performance				:	1,			// 0-Normal, 1-Hybrid speed/quality, 2-Optimizes image quality, 3-Optimizes transition speed // (Only works for Firefox/IE, not Webkit)
					image_protect			:	1,			// Disables image dragging and right click with Javascript
															   
					// Size & Position						   
					min_width		        :   0,			// Min width allowed (in pixels)
					min_height		        :   0,			// Min height allowed (in pixels)
					vertical_center         :   1,			// Vertically center background
					horizontal_center       :   1,			// Horizontally center background
					fit_always				:	0,			// Image will never exceed browser width or height (Ignores min. dimensions)
					fit_portrait         	:   1,			// Portrait images will not exceed browser height
					fit_landscape			:   0,			// Landscape images will not exceed browser width
															   
					// Components							
					slide_links				:	false,	// Individual links for each slide (Options: false, \'num\', \'name\', \'blank\')
					thumb_links				:	0,			// Individual thumb links for each slide
					thumbnail_navigation    :   0,			// Thumbnail navigation
					slides 					:  	[			// Slideshow Images';
					
					
					
					

        
       
        
    

        for($i=1; $i<=$images; $i++){

        	if($i==$images){$codecomma="";}else{$codecomma=",";}

        echo "\n{image : '../i/special/s".$i.".jpg', title : '', thumb : '', url : ''}$codecomma\n";
        

        }
    

 


					
					
													
					echo '],
												
					// Theme Options			   
					progress_bar			:	0,			// Timer for each slide							
					mouse_scrub				:	0
					
				});
		    });
		    
		</script>';



}

}

echo '<script src="'.$root.'js/ios-orientationchange-fix.js" type="text/javascript"></script>';

echo '</body>
</html>';



}

?>