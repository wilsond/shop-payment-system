<?php 

include("../includes/build.php"); 

pageheader('Flower Deliveries', 'delivery');

pagenav('deliveries');


?>

<div class="content">

<p>We specialise in stylish, modern hand-tied bouquets and arrangements, and always try to have something a little out of the ordinary to offer our customers.</p><p>We also have a selection of plants for inside and out, as well as a range of carefully selected gifts and cards.</p>
<p>
Bouquets and arrangements start from &pound;25.</p><p>We can incorporate the recipient&#8217;s favourite flowers or colours, if available, or you can leave it to one of our highly-trained florists to choose a selection of the best seasonal blooms.</p>
<p>There is free delivery in Poynton and a small charge for deliveries further afield.</p>
<p>
Please call us on 01625&nbsp;859525 or <a href="../contact/">visit the shop</a> to discuss your requirements.</p>
</div>
<div class="form">
<div class="areas">
<p>
We deliver to all of the locations listed here &#8211; for other areas please call us on 01625&nbsp;859525 for a quote.
</p>

<ul>
<li>
Adlington
</li>
<li>
Alderley Edge
</li>
<li>
Bollington
</li>
<li>
Bramhall
</li>
<li>
Bredbury
</li>
<li>
Cheadle
</li>
<li>
Cheadle Hulme
</li>
<li>
Davenport
</li>
<li>
Disley
</li>
<li>
Edgeley
</li>
<li>
Great Moor
</li>
<li>
Handforth
</li>
<li>
Hawk Green
</li>
<li>
Hazel Grove
</li>
<li>
Heald Green
</li>
<li>
Heaviley
</li>
<li>
High Lane
</li>
<li>
Higher Poynton
</li>
<li>
Macclesfield
</li>
<li>
Marple
</li>
<li>
Marple Bridge
</li>
<li>
Mellor
</li>
<li>
Mottram St Andrew
</li>
<li>
New Mills
</li>
<li>
Offerton
</li>
<li>
Pott Shrigley
</li>
<li>
Poynton
</li>
<li>
Prestbury
</li>
<li>
Romiley
</li>
<li>
Shaw Heath
</li>
<li>
Stepping Hill Hospital
</li>
<li>
Stockport
</li>
<li>
Strines
</li>
<li>
Styal
</li>
<li>
Tytherington
</li>
<li>
Wilmslow
</li>
<li>
Woodford
</li>
<li>
Woodsmoor
</li>
</ul>
</div>
</div>

<?

pagefooter('stretch,gef,form', 'i/bgimages/delivery.jpg');

?>

