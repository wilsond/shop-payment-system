<?php include("../includes/build.php"); 

//$navigationpage = '<p><a id="nextslide" class="load-item"></a></p>';

pageheader('Valentine&#8217;s Day 2013', 'special',$navigationpage);

pagenav('special');


?>

<div class="content" id="isspecial">
<h2>Mother&#8217;s Day<br />Sunday 10th March</h2>

<p>We love working with the best seasonal flowers, foliages and plants to create unique designs for everyone&#8217;s Mum.</p>

<!-- http://bit.ly/YUw4Yc for pdf -->

<p class="asbutton"><a onClick="_gaq.push(['_trackEvent', 'Mothers Day brochure', 'download', 'v1']);" href="handout.html">Download a Mother&#8217;s Day price list</a></p>

<p><a href="mailto:shop@greenearthflowers.com">Email us</a> or call us on 01625&nbsp;859525 to place your order.</p>

<p>We will be delivering on the Sunday (with no extra charge) or you can collect from the shop at your convenience.</p>

</div>


<?

pagefooter('gef, stretch', 'i/special/m1.jpg');

?>

