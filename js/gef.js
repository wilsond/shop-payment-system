if(typeof(console) === 'undefined') {
    var console = {}
    console.log = console.error = console.info = console.debug = console.warn = console.trace = console.dir = console.dirxml = console.group = console.groupEnd = console.time = console.timeEnd = console.assert = console.profile = function() {};
}

$(document).ready(function() {

// let us google analyse this site

$('a').click(function(){

var title = document.title;

var target = $(this).attr('href');

var whathappened = "On:"+title+", sent to:"+target;

_gaq.push(['_trackEvent', 'Page click', 'Click', whathappened]);


});




// is there a form attached to this page?

if($('.form').length>0){

var whichmessage = $('body').attr('id');

var messagetext = '<i>Send us a message</i>';

if(whichmessage =='delivery'){

messagetext='<i>Where we deliver</i>';


}

if(whichmessage =='weddings'){

messagetext='<i>Wedding enquiry form</i>';

}

var moveform = $('.form').html();

$('.form').remove();

$('#c').append('<div id="jqform">'+moveform+'</div>');

$('#jqform').prepend('<a href="#" class="openform icon close">close this</a>');



$('#c').addClass('hasform');

$('.content').append('<a href="#" class="openform icon email">'+messagetext+'</a>');


if(whichmessage =='delivery'){

//$('#jqform').append('<a href="#" class="openform hastext"><i>close this</i></a>');

}

if(whichmessage == 'contact'){



//$('.content').append('<a href="../weddings/index.php#openform" class="openform icon wedding"><i>Make a wedding enquiry</i></a>');


}



// wheel in the form/extra content. Set the margin-left according to the padding of the #c div

$('a.openform').click(function(){

// is a proper url

if($(this).attr('href')!="#"){return true;}

var whereis = parseInt($('#c').css('marginLeft'));

if (whereis==0){

$('#c').animate({marginLeft:'-400px'}, 500);

}else{

$('#c').animate({marginLeft:'0px'}, 500);

}


return false;

});

// check the url for the hash we add to pop open the wedding form from the contact us page

var hash = window.location.hash;

if( hash == "#openform") {

$('a.openform').click();

}



}

// end form manipulation




// weddings page - keep the wedding list in the centre of the window if possible.

var adjust = ($(window).height() - $('.weddingcarousel').height())/2;

if(adjust>0){$('.weddingcarousel').css('paddingTop', adjust+'px');}

$(window).resize(function() {
var adjust = ($(window).height() - $('.weddingcarousel').height())/2;

if(adjust>0){$('.weddingcarousel').css('paddingTop', adjust+'px');}

});

// end weddings page









// stretch the main content box 

function stretchcontent(){

var check = $(window).height();

var checkgrow = check*0.000082;

//console.log("cg"+checkgrow);

$('.nav').css('paddingTop', (check*checkgrow)+'px');

$('#jqform').css('paddingTop', ((check*checkgrow)+10)+'px');


//console.log('windowheight'+check);

var contentheight = $('#c').height();

var formheight = $('#jqform').outerHeight();

var useheight = 0;

if(contentheight>formheight){useheight=contentheight;}else{

useheight=formheight;

}

//console.log("content "+useheight+", document"+check);

if(useheight<=check){

$('#c').css("height",check+"px");

}else{
$('#c').css("height","auto");

}


}

var bodyid = $('body').attr('id');

if(bodyid!="index"){

stretchcontent();

$(window).resize(function() {

//console.log('resize event');

stretchcontent();

});

}

// end content stretch


// do scrolling names on the wedding carousel page

$('.weddingcarousel ul li a').hover(function() {


  $(this).children('span').stop(true, true).animate({'left':0}, 500);
  
  }, function(){
  
    $(this).children('span').stop(true, true).animate({'left':'-300px'}, 300);

  });
  

$('a#hidecontent').css('left', '-32px');  

$('#weddingp a#nextslide').click(function(){


$('#c').animate({'marginLeft': '-390px'}, 800,function(){
$('a#hidecontent').animate({'left':'0px'}, 100);

});


});

$('a#hidecontent').click(function(){

$(this).animate({'left': '-32px'}, 100, function(){

$('#c').animate({'marginLeft': '0'}, 700);


});

});


// validate our forms


if($('form').length>0){

$('form').validate({


validClass: "success",
messages: {
     sendername: "Please enter your name",
     senderemail: {
       required: "Please enter an email address",
       email: "Your email address does not seem to be valid"
     },
     senderphone: "Please enter a phone number",
     sendermessage: "Please write us a message",
     
	weddingname: "Please enter your name",
     weddingemail: {
       required: "Please enter an email address",
       email: "Your email address does not seem to be valid"
     },
     weddingphone: "Please enter a phone number",
     weddingdate: "Please let us know what time of year you&#8217;re getting married",
     weddinglocation: "Please let us know if you&#8217;ve booked a venue or where you&#8217;re thinking of getting married",
     weddingstyle:"Please let us know if you have a particular style in mind, or if you&#8217;re happy to discuss this with us"
     
     

},
showErrors:function(errorMap,errorList){
   
    this.defaultShowErrors();
    stretchcontent();
},




submitHandler: function(form) { 


//event.preventDefault(); 
        
var dataString = $(form).serialize();
  
//console.log(dataString);

$.post('../mail.php', dataString, function(data) {
  $('form').remove();
$("html.body").scrollTop(0);
  $('#jqform').append("<p class=\"success\">Thank you for your email: it&#8217;s been sent.</p>");
  stretchcontent();
  
});

}

});

}


});







