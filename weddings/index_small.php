<?php 

include("../includes/build_small.php"); 

include("wedding_list.php"); 

$weddinghtml = '<div class="weddingcarousel"><ul>';

foreach($weddings as $w){


//page 0
//names 1 x
//locations 2 x
//description - season, style, colour, flower types 3 xx
//testimonial 4 x
//photographer credit &amp; link 5 x
//cover image 6
//got photos? 7

$slug = str_replace(" &amp; ", "-", strtolower($w[1]));
$weddinghtml .=  '<li>'.$slug.'/'.$w[6];

$weddinghtml .= '<img src="../i/weddingthumb/'.$slug.'.jpg" alt="'.$slug.'" />';


$weddinghtml .= '<a href="wedding_small.php?w='.$slug.'"><span class="shade">';

$weddinghtml .= str_replace('&amp;', '<i>&amp;', $w[1]).'</i>';

$weddinghtml .= '</span></a>';


$weddinghtml .= '<b>&gt;</b></li>';


}


$weddinghtml .='</ul></div>';


pageheader('Weddings', 'weddings', $weddinghtml);

pagenav('weddings');


?>


<div class="content">
<p>
Your wedding day is as individual as you are.
</p>
<p>
At Green Earth Flowers, we pride ourselves on our bespoke approach to providing the perfect flowers for your special day. Combining your ideas with our expertise and enthusiasm, your wedding flowers will be unique to you, tailored to your exact requirements, whatever the location or budget.
</p>
<p class="rest">
The first step is to book an appointment to come and meet us for a free, no obligation wedding consultation, after which we will send you a detailed quotation. <!-- We have decorated venues in Cheshire, Derbyshire, Manchester and across the North West.--></p>

<p class="hm">Get in touch by filling in our wedding enquiry form, or call us on 01625 859525, and let us show you how we can make your wedding day truly memorable.
</p>
</div>

<div class="form">
<form action="../mail.php" method="post">
<p>
<label>Name</label>
<input name="weddingname" type="text" size="40" class="required" minlength="2" />
</p>
<p>
<label>Email address</label>
<input name="weddingemail" type="text" size="40" class="required email" />
</p>
<p>
<label>Phone Number</label>

<input name="weddingphone"  type="text" size="40" class="required" minlength="6" />
</p>

<p>
<label>When are you getting married?</label>

<input  name="weddingdate"  type="text" size="40" class="required" minlength="2" />
</p>

<p>
<label>Where are you getting married?</label>

<input  name="weddinglocation"  type="text" size="40" class="required" minlength="2" />
</p>
<p>

<label>Do you have a particular style<br />or colour in mind?</label>


<textarea name="weddingstyle" rows="5" cols="40" class="required"></textarea>
</p>
<p>
<button type="submit">Send
</button>
</p>
</form>

</div>

<?

pagefooter('gef,form');

?>

