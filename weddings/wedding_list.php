<?php

$weddings=Array(

Array(
1,"Laura &amp; James","Hilltop Country House, Prestbury, Cheshire","","Hi Laura</p><p>It has totally just hit me that I never said<br />THANK YOU<br />for my wedding flowers. &#8230; They were totally what I had envisaged and everyone commented on how beautiful they were. You made the venue look even more special.</p><p>I just wanted to say thanks and I cannot believe that I forgot!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!</p><p>Laura Cordingley</p>","Joe Gardner Photography http://joegardnerphotography.com/","2a","y",12,1,"all")
,
Array(
1,"Ali &amp; Graeme","Didsbury House Hotel &amp; Eleven Didsbury Park, Manchester","","none","Bailey:Image http://www.baileyimage.co.uk/blog/",6,"y",12,1),

Array(
2,"Laura &amp; Simon","St Christopher&#8217;s, Pott Shrigley &amp; Alderley Edge Hotel, Cheshire","","Dear Laura and your team,</p><p> Thankyou so much for being part of our wedding celebrations for Laura and Simon last week. The day was wonderful and just perfect.</p><p>There are so many people who helped in making it such a perfect day, and your flowers were part of it. They were absolutely beautiful &#8211; a beautiful day.</p><p>A big thanks again.</p><p>Kind regards and best wishes,<br />Sheila Jenkins.</p><p class=\"start2\">Dear Laura and team,</p><p>Just wanted to drop you a note to thank you for my lovely, gorgeous wedding flowers. They were so much more fantastic than I had imagined and the colour was a 100% match!</p><p> They were totally perfect and I had so many comments about them. Thankyou for all your help before the day, with all those lavender stems and on the day &#8211; I know time was tight!</p><p>I&#8217;m sure Mum has told you how perfect the day was!</p><p>Thankyou,<br />love Laura and Simon xx","Jill Thorning-Jensen http://www.jtjphoto.co.uk/",9,"y",12,1),

Array(
1,"Gemma &amp; Paul","Sandhole Oak Barn, Congleton, Cheshire","","Dear Laura,</p><p>Just a quick note to say thankyou so much for the absolutely beautiful flowers for our wedding. They really were perfect, from the bouquets to the table decorations.</p><p>With love,<br />Gemma &amp; Paul x","Shoot Photography http://www.shoot-lifestyle.co.uk/","3b","y",8,8, "all"),


Array(
1,"Nicola &amp; Matt","The Ashes, Endon, Staffordshire","","none","Andy Wardle http://www.andywardle.com/",13,"y",18,6, "all"),

Array(
2,"Becky &amp; Alex","Heaton House Farm, Cheshire","","Dear Laura and all at Green Earth Flowers,</p><p>Thank you so much for all your hard work making the flowers for our wedding so beautiful! They really brought everything together &#8211; I couldn&#8217; t have hoped for anything as lovely!</p><p>All your help in the run up to the big day made such a difference. I am missing all the preparations!</p><p>Lots of love,<br />Becky and Alex xxx","none","1b","y", 9, 1),

Array(
2,"Heather &amp; Adam","Peckforton Castle, Cheshire","","Hi Laura,</p><p>We just wanted to say thankyou for the beautiful flowers you provided, they were exactly what we wanted, the colours were so rich and warm.</p><p>They were absolutely beautiful, so much so that our guests where arguing over who was taking the centrepieces home!</p><p>Thankyou for making our wedding day special &#8211; we had a beautiful, memorable, perfect day which would not have been possible without your help.</p><p>Kind Regards,<br />Heather and Adam","David Ho Photography http://davidho.co.uk/","1b","y",8,1),


Array(

1,"Kate &amp; Jack","St Christopher&#8217;s, Pott Shrigley &amp; Hallmark Hotel, Handforth, Cheshire","","none","Pixies in the Cellar http://www.pixiesinthecellar.co.uk/","2a","y", 5, 1, "all"),

Array(
1,"Helen &amp; Alex","Thornton Manor, Wirral, Merseyside","","Hi Laura,</p><p>Thank you ever so much &#8211; the flowers were absolutely beautiful, everything looked so amazing!!</p><p>My mum could not bear to leave any behind this morning, so she now has a house full of flowers.</p><p>Thank you again &#8211; you and you team are amazing,</p><p>Helen x","Ian MacMichael http://www.ianmacmichael.co.uk/","2a","y",11,4),



Array(
1,"Mel &amp; James","Sandhole Oak Barn, Congleton, Cheshire","","Hi Laura,</p><p>Just wanted to drop you a quick note to say a huge thanks for doing our wedding flowers, they were amazing!  The swag above the front entrance door was especially lovely!</p><p>Everyone commented on how gorgeous the flowers were, especially the autumnal colours! We had a fantastic day, perfect in every way&#8230; we wouldn&#8217; t change a thing!</p><p>Thanks again,<br />Mel.","Jay Cain http://www.jaycain.co.uk/","5b","y",7,1),

Array(
1,"Sarah &amp; Jamie","Heaton House Farm, Cheshire","","Dear Laura,</p><p>Thank you so much for the brilliant flowers and centrepieces for our wedding. I have never seen anything as gorgeous as our bouquets and buttonholes. PERFECT.</p><p>The big arrangement was sensational, I had to remove it from the fireplace at New Year &#8211; with a lump in my throat! The Brussels sprouts were a big hit too &#8211; everyone &#8216;got it&#8217;. It took me a long time to find you during the wedding planning months.</p><p>Thank God I did &#8211; no-one else had your fabulous creativity.</p><p>Big thanks and warm wishes,<br />Sarah and Jamie xxxx","StudioFiveFour Photography http://studiofivefour.com/","6b","y",4,1)








);

/* 


/* Array(
2,"Amy &amp; John","Sandhole Oak Barn, Congleton, Cheshire","","<p>Dear Laura,</p><p>A huge thankyou for providing our beautiful flowers for our wedding at Sandhole Farm &#8211; they were perfect! My bouquet was stunning and exactly what I wanted.</p><p>The bridesmaids&#8217; bouquets and all buttonholes were fabulous. The cake was amazing and we had so many compliments.</p><p>Many, many thanks,<br />love Amy and John xx<br />(The new Mr &amp; Mrs Allen)</p>","Paparella http://paparella.co.uk/","1a","y", 9, 3),

Array(
2,"Nicola &amp; John","Church of St Peter and St Paul, Deddington &amp; Blenheim Palace, Oxfordshire","","none","none",4,"y",8,4
), 
Array(
2,"Katie &amp; Adam","Salford Cathedral &amp; Radisson Edwardian Hotel, Manchester","","none","Victoria Hamilton http://www.victoriahamilton.co.uk/","4b","y",10,1),


Array(
1,"Sarah &amp; Arran","The Avenue Methodist Church, Sale &amp; Belle Epoque, Knutsford","","To Laura and all the staff at Green Earth, We just wanted to say an absolutely enormous thank you for the out of this world flowers you provided for our wedding. My bouquet was honestly one of my favourite things of the whole day. You completely exceeded my expectations! Thankyou so much, Sarah and Arran xxxx","CG Weddings http://www.cgweddings.co.uk/main.php",10,"y"),
Array(
2,"Sarah &amp; David","St Bartholomew&#8217;s, Wilmslow &amp; Belle Epoque, Knutsford","","Laura, Thankyou for doing such stunningly beautiful flowers for our wedding in December. The bouquets, buttonholes and display exceeded what we wanted. I was so upset to leave for honeymoon, leaving behind my bouquet! Thankyou. Love Sarah and David Moores xxx","",6,"y"),
Array(
1,"Alex &amp; Jonathan","Great John St Hotel, Manchester","","Hi Laura, Jon and I just wanted to say a big thank you for all your hard work that went into to our wonderful flowers. They were perfect and the colours looked amazing (especially with my dress!!). I couldn&#8217; t have hoped for anything more! Thanks again! Alex and Jon","Tobiah Tayo http://www.tobiahtayo.com/","4a","y"),
*/

?>
